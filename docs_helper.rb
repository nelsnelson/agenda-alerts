#! /usr/bin/env ruby

DOCS_URL = 'http://austin.siretechnologies.com/sirepub/agdocs.aspx?doctype=agenda&itemid='
DOC_URL = 'http://austin.siretechnologies.com/sirepub/'

module DocsHelper
  def docs(item_id)
    doc_urls = nil
    page = webpage "#{DOCS_URL}#{item_id}"
    doc_anchors = Nokogiri::HTML::DocumentFragment.parse(page).css('table.regtable').css('tr').css('td.tabledata').css('a')
    doc_urls = doc_anchors.collect do |anchor|
      href = anchor.attr('href')
      "#{DOC_URL}#{href}"
    end unless doc_anchors.empty?
    doc_urls
  end
end

