#! /usr/bin/env bash

apt-get install ruby
apt-get install ruby-dev ri rdoc irb
apt-get install libreadline-ruby1.8 libruby1.8 libopenssl-ruby
apt-get install rubygems
#
# Mongo
#
gem install mongo
gem install bson-ext
#
# Nokogiri
#
apt-get install libxslt-dev libxml2-dev
gem install nokogiri
#
# JSON
#
gem install json
#
# Chronic
#
gem install chronic

echo ""
echo "Finished installing"
