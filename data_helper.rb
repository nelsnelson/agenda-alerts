#! /usr/bin/env ruby

require 'json'
require './meetings_helper.rb'
require './items_helper.rb'

module DataHelper
  include MeetingsHelper
  include ItemsHelper
end

class App
  include DataHelper

  def main
    data = []
    future_meetings.each { |meeting|
      items = agenda_items meeting
      data << items
    }
    #puts data.to_json
    #for entry in data
    #  for key, value in entry
    #    puts "#{key}: #{value}"
    #  end
    #end
  rescue Interrupt => ex
    puts "Done"
  end
end

App.new.main if __FILE__ == $0

