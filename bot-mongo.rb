#! /usr/bin/env ruby

require 'rubygems' if RUBY_VERSION =~ /1.8/
require 'mongo'
gem 'bson_ext'

require './data_helper.rb'

DEFAULT_DATABASE = 'agendas'
DEFAULT_COLLECTION = 'agendas'

class AgendaPKFactory
  def create_pk(doc)
    return doc if doc[:_id]
    doc.delete(:_id) # in case it exists but the value is nil
    doc['_id'] ||= BSON::ObjectId.new
    doc
  end
end

AGENDA_PRIMARY_KEYS = AgendaPKFactory.new

module MongoHelper
  def count(collection)
    puts "Counting objects in collection #{collection.name}..."
    puts
    puts "There are #{collection.count} objects in the #{collection.name} collection"
    collection.count
  end
  def collections(mongo_client, db)
    return unless mongo_client and db
    mongo_client.database_names.each { |name|
      db.collections.each { |c|
        puts "#{name} - #{c.name}"
      }
    }
  end
  def default_collection(db)
    db.collections.find { |c| c.name =~ /^#{DEFAULT_COLLECTION}$/ }
  end
  def reset(collection)
    if collection
      puts "Removing collection #{collection.name}"
      collection.drop
    end
  end
  def default_collection(db)
    collection = db.collections.find { |c| c.name =~ /^#{DEFAULT_COLLECTION}$/ }
    return collection if collection
    Mongo::Collection.new(DEFAULT_COLLECTION, db, { :pk => AGENDA_PRIMARY_KEYS })
  end
  def test_write_capability(host, db)
    collection = nil
    begin
      puts "Testing write capability..."
      collection = default_collection db
      if verify_write_capability collection
        puts "Collection is open for writes - #{collection.name}"
      else
        raise "Not writable"
      end
    rescue Exception => ex
      if ex.message == "Not writable"
        puts "Mongo node #{host} does not allow writes (#{ex.message})"
      else
        raise ex
      end
    end
    collection
  end
  def verify_write_capability(collection)
    cursor = nil
    begin
      puts
      puts "Attempting to insert test object"
      id = collection.insert({})
      cursor = collection.find({"_id" => id})
      if cursor.has_next?
        puts "Successfully inserted test object: #{id}"
      else
        puts "Failed to insert test object: #{id}"
      end
      puts

      puts "Attempting to delete test object: #{id}"
      result = collection.remove({"_id" => id})
      if result[:err]
        puts "Failed to delete test object #{id} - #{result[:err]}"
      else
        puts "Successfully invoked delete operation with test object: #{id}"
      end

      puts "Attempting to find deleted test object: #{id}"
      cursor = collection.find({"_id" => id})
      if cursor.has_next?
        puts "Failed to not find deleted test object: #{id}"
      else
        puts "Successfully did not find deleted test object #{id}"
      end
      puts
      return true
    rescue Exception => ex
      puts "Problem verifying write capability: #{ex.message}"
    ensure
      cursor.close if cursor
    end
    return false
  end
end

module BotHelper
  def maybe_notify
    obj = collection.find_one
    potentially_new_meeting = new_meeting?
    if potentially_new_meeting
      possibly_new_agenda_id = potentially_new_meeting[:agenda_id]
      if obj and obj['agenda_id'] == possibly_new_agenda_id
      else
        notify_agenda_alerts new_meeting[:agenda_id] if new_meeting
      end
    end
  end

  def notify_agenda_alerts(agenda_id)
    puts "Notifying Agenda Alerts about new meeting(s)..."
    `curl -sO http://50.56.172.7/botapi.php?Agenda=#{agenda_id}` # TODO FIX/CONFIRM
  end

  def scan_into(collection)
    obj = collection.find_one
    if obj
      puts "Got a sample object"
      agenda_id = obj['agenda_id']
      puts "Agenda ID: #{agenda_id}"
      if agenda_id and not agenda_id.empty?
        puts "Checking for new meeting..."
        potentially_new_meeting = new_meeting?
        if potentially_new_meeting
          possibly_new_agenda_id = potentially_new_meeting[:agenda_id]
          if agenda_id == possibly_new_agenda_id
            puts "Agenda already exists - #{agenda_id}"
            return
          else
            puts "Found a new meeting!"
          end
        end
      else
        puts "No existing agendas"
      end
    end

    puts "Scanning new meetings..."

    agendas = meetings
    agendas.each { |meeting|
      items = agenda_items meeting
      items.each { |item|
        puts "Trying to insert #{item.to_json} into Mongo DB"
        id = collection.insert(item)
        puts "Inserted item #{id}"
      }
    }

    count collection
  end
end

class App
include MongoHelper
include BotHelper

def main
  mongodb_hosts = [
    # so-called "data02.atxhackathon-mongo.io"
    '166.78.181.9',
    # so-called "primary.atxhackathon-mongo.io"
    '166.78.181.194',
    # so-called "ata01.atxhackathon-mongo.io"
    '166.78.181.230'
  ]
  mongo_client = nil
  db = nil
  collection = nil
  for host in mongodb_hosts
    begin
      puts "Establishing connection to Mongo DB #{host}"
      mongo_client = Mongo::MongoClient.new(host, 27017)
      db = mongo_client.db(DEFAULT_DATABASE, :pk => AGENDA_PRIMARY_KEYS)
      puts "Connection established"

      collection = test_write_capability host, db
    rescue Exception => ex
      puts "Mongo replicant set fux0red itself (#{ex.message}), trying another host"
    end
    if mongo_client and db and collection
      if mongo_client.primary_pool
        puts "Established a connection to #{mongo_client.host}:#{mongo_client.port}"
      else
        puts "Established a connection to #{mongo_client.host_port.join(':')}"
      end
      break
    end
    puts
  end
  unless mongo_client and db and collection
    puts "A connection to the Mongo DB replicant set could not be established"
    exit
  end

  #
  # Commands
  #

  if ARGV.include? '--update'
    exit unless ARGV.include? '--wet-run'
  end

  if ARGV.include? '--sample'
    puts "Sampling collection #{collection.name}"
    collection.find({}, {:limit => 5}).each do |obj|
      puts obj.inspect
    end
    exit unless ARGV.include? '--wet-run'
  end

  if ARGV.include? '--list-collections' or ARGV.include? '--collections'
    collections mongo_client, db
    exit unless ARGV.include? '--wet-run'
  end

  if ARGV.include? '--reset'
    unless ARGV.include? '--wet-run'
      puts
      puts "Are you sure that you want to reset the Mongo DB #{collection.name} collection?"
      puts "If so, please run the command again with the option --wet-run specified."
      exit
    end
    collections mongo_client, db
    collection = default_collection db
    reset collection
    collection = default_collection db
    count collection
    scan_into collection
    exit
  end

  if ARGV.include? '--count'
    count collection
    exit unless ARGV.include? '--wet-run'
  end

  if ARGV.include? '--dry-run'
    exit
  end

  n0 = count collection
  notify if scan_into(collection) > n0
end
end

App.new.main if __FILE__ == $0

