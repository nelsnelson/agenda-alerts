#! /usr/bin/env ruby

require 'rubygems' if RUBY_VERSION =~ /1.8/
require 'redis'

require './data_helper.rb'

module RedisHelper
  def count(store)
    puts "Counting objects in store #{store}..."
    puts
    puts "There are #{@redis.hlen(store)} objects in the #{store} store"
    @redis.hlen(store) 
  end
  def collections(store)
    return unless @redis and store
    @redis.hkeys.each { |name|
      @redis.hget(store, name).each { |c|
        puts "#{name} - #{c.name}"
      }
    }
  end
  def reset(store)
    if store
      puts "Removing store #{store}"
      @redis.del store
    end
  end
  def default_store
    @redis.hgetall @redis.keys('*').first
  end
  def dummy(store)
    @redis.hmset store, 'test2', 'Hello, world test2!' 
    @redis.hmset store, 'test3', 'Good-bye, world test3!'
    @redis.hmset store, 'test-hash1', { :id => 1, :message => 'Good-bye, world test3!' }
  end
  def test_write_capability
    store = nil
    begin
      puts "Testing write capability..."
      store = default_store
      if verify_write_capability store
        puts "Store is open for writes - #{store}"
      else
        raise "Not writable"
      end
    rescue Exception => ex
      if ex.message == "Not writable"
        puts "Mongo node #{host} does not allow writes (#{ex.message})"
      else
        raise ex
      end
    end
    store
  end
  def verify_write_capability(collection)
    cursor = nil
    begin
      puts
      puts "Attempting to insert test object"
      id = collection.insert({})
      cursor = collection.find({"_id" => id})
      if cursor.has_next?
        puts "Successfully inserted test object: #{id}"
      else
        puts "Failed to insert test object: #{id}"
      end
      puts

      puts "Attempting to delete test object: #{id}"
      result = collection.remove({"_id" => id})
      if result[:err]
        puts "Failed to delete test object #{id} - #{result[:err]}"
      else
        puts "Successfully invoked delete operation with test object: #{id}"
      end

      puts "Attempting to find deleted test object: #{id}"
      cursor = collection.find({"_id" => id})
      if cursor.has_next?
        puts "Failed to not find deleted test object: #{id}"
      else
        puts "Successfully did not find deleted test object #{id}"
      end
      puts
      return true
    rescue Exception => ex
      puts "Problem verifying write capability: #{ex.message}"
    ensure
      cursor.close if cursor
    end
    return false
  end
end

module BotHelper
  def maybe_notify
    obj = store.find_one
    potentially_new_meeting = new_meeting?
    if potentially_new_meeting
      possibly_new_agenda_id = potentially_new_meeting[:agenda_id]
      if obj and obj['agenda_id'] == possibly_new_agenda_id
      else
        notify_agenda_alerts new_meeting[:agenda_id] if new_meeting
      end
    end
  end

  def notify_agenda_alerts(agenda_id)
    puts "Notifying Agenda Alerts about new meeting(s)..."
    #`curl -sO http://50.56.172.7/botapi.php?Agenda=#{agenda_id}` # TODO FIX/CONFIRM
  end

  def scan_into(store)
    obj = @redis.hgetall(store).first
    if obj
      puts "Got a sample object"
      agenda_id = obj['agenda_id']
      if agenda_id and not agenda_id.empty?
        puts "Agenda ID: #{agenda_id}"
        puts "Checking for new meeting..."
        potentially_new_meeting = new_meeting?
        if potentially_new_meeting
          possibly_new_agenda_id = potentially_new_meeting[:agenda_id]
          if agenda_id == possibly_new_agenda_id
            puts "Agenda already exists - #{agenda_id}"
            return
          else
            puts "Found a new meeting!"
          end
        end
      else
        puts "No existing agendas"
      end
    end

    puts "Scanning new meetings..."

    agendas = meetings
    agendas.each { |meeting|
      items = agenda_items meeting
      items.each { |item|
        puts "Trying to insert #{item.to_json} into redis"
        #id = store.insert(item)
        id = item[:id]
        @redis.hset store, id, item.to_json
        puts "Inserted item #{id}"
      }
    }

    count store
  end
end

class App
include RedisHelper
include BotHelper

def main
  redis_hosts = [
    'localhost'
  ]
  @redis = nil
  for host in redis_hosts
    begin
      puts "Establishing connection to redis #{host}"
      @redis = Redis.current = Redis.new(:host => host, :port => 6379) 
      #@redis = Redis.current = Redis.new(:host => host, :port => 6379, :password = 'password') 
      puts "Connection established"
    rescue Exception => ex
      puts "Redis fux0red itself (#{ex.message}), trying another host"
    end
    if @redis
      host = @redis.client.host
      port = @redis.client.port
      puts "Established a connection to a redis server #{host}:#{port}"
      store = 'items'
      break
    end
    puts
  end
  unless @redis 
    puts "A connection to Redis could not be established"
    exit
  end

  if ARGV.include? '--debug'
    puts "Redis:"
    puts "  methods: #{@redis.class.instance_methods.sort.join(',')}"
    puts "Client:"
    puts "  methods: #{@redis.client.class.instance_methods.sort.join(',')}"
  end

  #
  # Commands
  #

  if ARGV.include? '--update'
    exit unless ARGV.include? '--wet-run'
  end

  if ARGV.include? '--sample'
    puts "Sampling store items"
    items = @redis.hgetall(store)
    if items.empty?
      puts "Empty store"
      exit
    end
    i = 0
    items.each do |key, value|
      obj = JSON.parse(value)
      puts "#{key}: #{obj.inspect}"
      #item = Marshal.load(@redis["#{store}-#{key}"])
      #puts "#{i}) #{item}"
      break if i > 5
      i = i + 1
    end
    exit unless ARGV.include? '--wet-run'
  end

  if ARGV.include? '--list-stores' or ARGV.include? '--stores'
    stores
    exit unless ARGV.include? '--wet-run'
  end

  if ARGV.include? '--dummy'
    dummy store
    exit
  end

  if ARGV.include? '--reset'
    unless ARGV.include? '--wet-run'
      puts
      puts "Are you sure that you want to reset the Redis #{store} store?"
      puts "If so, please run the command again with the option --wet-run specified."
      exit
    end
    reset store
    exit
  end

  if ARGV.include? '--count'
    count store
    exit unless ARGV.include? '--wet-run'
  end

  if ARGV.include? '--dry-run'
    exit
  end

  exit unless ARGV.include? '--wet-run'
  n0 = count store
  scan_into 'items'
  #notify if scan_into('test') > n0
end
end

App.new.main if __FILE__ == $0

