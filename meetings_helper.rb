#! /usr/bin/env ruby

require './web_helper.rb'
require './time_helper.rb'

MEETINGS_URL = 'http://www.austintexas.gov/department/city-council/council-meetings'

AGENDA = '<iframe id="agviewmain" name="agviewmain" src="(.*)" style=".*" frameborder="0"></iframe>'
ANCHOR = '<a class="edims_cmic" target="[\w_]+" href="(https?\:\/\/.*meetid\=)(\d+)[^"]*">.*<\/a>'
ANCHOR_DRAFT = '<a class="edims_cmic" target="[\w_]+" href="(https?\:\/\/.*meetid\=)(\d+)[^"]*">.*Draft Agenda.*<\/a>'

module MeetingsHelper
  include WebHelper
  include AgendaTimeHelper

  def agenda(meeting_url)
    page = webpage meeting_url
    page.lines.grep(/#{AGENDA}/) {
      resource = "#{$1}".gsub(/&amp;/, '&')
      return "http://austin.siretechnologies.com/sirepub/#{resource}"
    }
  end

  def new_meeting?
    page = webpage MEETINGS_URL
    result = page.lines.collect { |meeting| "#{$1}" if meeting.match(/#{ANCHOR}/) }.compact.first
    return false unless result
    agenda_url = agenda(result)
    agenda_time = agenda_time agenda_url
    agenda_time = Time.now if not agenda_time
    {
      :id => "#{$2}",
      :url => "#{$1}",
      :agenda_id => agenda_time.strftime('%Y-%m-%d'),
      :agenda_url => agenda_url
    }
  end

  def future_meetings
    page = webpage MEETINGS_URL
    results = page.lines.grep(/#{ANCHOR_DRAFT}/) {
      meeting_url = "#{$1}#{$2}"
      agenda_url = agenda meeting_url
      agenda_time = agenda_time agenda_url
      agenda_time = Time.now if not agenda_time
      {
        :id => "#{$2}",
        :url => "#{$1}#{$2}",
        :agenda_id => agenda_time.strftime('%Y-%m-%d'),
        :agenda_url => agenda_url
      }
    }
    results
  end

  def meetings
    page = webpage MEETINGS_URL
    results = page.lines.grep(/#{ANCHOR}/) {
      meeting_url = "#{$1}#{$2}"
      agenda_url = agenda meeting_url
      agenda_time = agenda_time agenda_url
      agenda_time = Time.now if not agenda_time
      {
        :id => "#{$2}",
        :url => "#{$1}#{$2}",
        :agenda_id => agenda_time.strftime('%Y-%m-%d'),
        :agenda_url => agenda_url
      }
    }
    results
  end
end

class App
  include MeetingsHelper

  def main
    meetings.each { |meeting|
      puts meeting[:id]
      puts "  #{meeting[:url]}"
      puts "  #{meeting[:agenda_id]}"
      puts "  #{meeting[:agenda_url]}"
    }
  rescue Interrupt => ex
    puts "Done"
  end
end

App.new.main if __FILE__ == $0

