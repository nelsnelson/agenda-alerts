

# [Meeting 423](http://austin.siretechnologies.com/sirepub/mtgviewer.aspx?meetid=423&amp;doctype=agenda)

## [Agenda 2013-06-04](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA)


### Austin Energy

  * [Item24882](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24882)
    - Content: 2. Authorize negotiation and execution of an agreement with Seton Healthcare, to provide a performance-based incentive for the generation of solar energy at its facility located at 4900 Mueller Blvd., Austin, Texas 78723, over a 10-year period for an estimated $7,325 per year, for a total amount not to exceed $73,250.
    - Zip code: [78723](https://maps.google.com/maps?q=78723)
  * [Item24883](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24883)
    - Content: 3. Authorize negotiation and execution of an agreement with St. David s Episcopal Church, to provide a performance-based incentive for the generation of solar energy at its facility located at 308 E. 8th St., Austin, Texas 78701, over a 10-year period for an estimated $26,969 per year, for a total amount not to exceed $269,690.
    - Zip code: [78701](https://maps.google.com/maps?q=78701)
  * [Item24884](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24884)
    - Content: 4. Approve issuance of a rebate to Arts Apartments South Austin, LLC, for performing comprehensive energy efficiency upgrades at the Arts Apartments at South Austin located at 400 West St. Elmo Rd., Austin, Texas 78745, in an amount not to exceed $99,000.
    - Zip code: [78745](https://maps.google.com/maps?q=78745)
  * [Item24886](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24886)
    - Content: 5. Approve issuance of a rebate to Arts Apartments at Turtle Creek, LLC, for performing comprehensive energy efficiency upgrades at the Arts Apartments at the Turtle Creek located at 714 Turtle Creek Blvd., Austin, Texas 78745, in an amount not to exceed $71,500.
    - Zip code: [78745](https://maps.google.com/maps?q=78745)
  * [Item24887](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24887)
    - Content: 6. Approve issuance of a rebate to H&H Texas Partners, LTD, for performing comprehensive energy efficiency upgrades at the Timbercreek Apartments located at 614 South 1st St., Austin, Texas 78704, in an amount not to exceed $132,000.
    - Zip code: [78704](https://maps.google.com/maps?q=78704)
  * [Item24888](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24888)
    - Content: 7. Approve issuance of a rebate to Capri Capital for performing duct diagnostics and improvements at the Stonegate Apartments located at 10505 South IH-35, Austin, Texas 78747, in an amount not to exceed $146,974.
    - Zip code: [78747](https://maps.google.com/maps?q=78747)

### Austin Water Utility

  * [Item25047](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25047)
    - Content: 8. Approve Service Extension Request No. 3151 for water service to a 37 acre tract at 9300 W SH 71, located within the Drinking Water Protection Zone and partially in the City's 2-mile extraterritorial jurisdiction. Related to Item #9.
  * [Item25048](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25048)
    - Content: 9. Approve Service Extension Request No. 3152 for wastewater service to a 37 acre tract at 9300 W SH 71, located within the Drinking Water Protection Zone and partially in the City's 2-mile extraterritorial jurisdiction. Related to Item #8.

### Aviation

  * [Item24412](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24412)
    - Content: 10. Authorize negotiation and execution of an amendment to the lease and hangar facility development agreement with Ascend AUS, LLC to require the Department of Aviation to relocate a Federal Aviation Administration signal cable, refund tenant a total amount of $173,804.00, and modify other lease terms consistent with the mediated settlement agreement dated July 17, 2012.

### Contract Management

  * [Item24564](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24564)
    - Content: 11. Authorize execution of change order #3 to the construction contract with DENUCCI CONSTRUCTORS, for the Pedernales Street Reconstruction and Utility Adjustment from 6th Street to Webberville Road (Group 7) Project, an ACCELERATE AUSTIN PROJECT, in the amount of $65,000 plus additional contingency in the amount of $88,459.90, for a total contract amount not to exceed $2,066,117.80. ( Notes: This contract was awarded in compliance with City Code Chapter 2-9A (Minority Owned and Women Owned Business Enterprise Procurement Program) with 4.45% MBE and 1.54% WBE subcontractor participation to date. )
  * [Item24135](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24135)
    - Content: 12. Authorize negotiation and execution of an amendment to the professional services agreement with THE BROUSSARD GROUP, INC., DBA TBG PARTNERS, A TEXAS CORPORATION, for architectural and engineering services for the Town Lake Metropolitan Park- Auditorium Shores event lawn, off-leash area and central lawn area of Town Lake Park north of Riverside Drive in the amount of $400,000, for a total contract amount not to exceed $2,982,163.35. Related to Items #25 and #26.

### Economic Growth and Redevelopment Services

  * [Item24628](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24628)
    - Content: 13. Authorize the negotiation and execution of Amendment No. 1 to the Interlocal Agreement for Services to Develop an Analytic Tool for Sustainable Communities Regional Planning with the Capital Area Council of Governments to increase the amount payable to the City by $12,500 to employ summer interns, for a total contract amount not to exceed $205,507. Related to Item #27.

### Emergency Medical Services

  * [Item25027](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25027)
    - Content: 14. Authorize negotiation and execution of an amendment to the existing interlocal cooperation agreement between the City and Travis County to provide additional emergency medical services in an area of Travis County outside the City's corporate limits for a period beginning June 1, 2013 through September 30, 2013 in exchange for payment of $188,838 by Travis County. Related to Item #15.
  * [Item25028](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25028)
    - Content: 15. Approve an ordinance amending the Fiscal Year 2012-2013 General Fund Emergency Medical Services Department Operating Budget (Ordinance No. 20120910-001) to increase revenue in the amount of $188,838 and increase expenses in the amount of $188,838 to add six new EMS Medic I full-time equivalent positions. Related to Item #14.
  * [Item25029](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25029)
    - Content: 16. Approve an ordinance establishing classifications and positions in the classified service of the Emergency Medical Services Department; eliminating and creating certain positions in the classified service of the Emergency Medical Services Department; and repealing ordinance no. 20130328-015 relating to Emergency Medical Services Department classifications and positions.

### Government Relations

  * [Item24641](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24641)
    - Content: 17. Approve a resolution adopting the City's federal legislative program for the 113th Congress, including Fiscal Year 2014 appropriations requests.

### Health and Human Services

  * [Item24561](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24561)
    - Content: 18. Authorize negotiation and execution of a social services contract with SKILLPOINT ALLIANCE, for a twelve-month term beginning on July 1, 2013 and ending on June 30, 2014, in an amount not to exceed $150,000, with no extension options.
  * [Item24566](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24566)
    - Content: 19. Authorize negotiation and execution of an interlocal agreement with THE UNIVERSITY OF TEXAS HEALTH SCIENCE CENTER AT HOUSTON for a 12 month term from July 1, 2013 through June 30, 2014 for evaluation of a healthy-based initiative to address the needs of economically disadvantaged households in Austin in a total amount not to exceed $50,000.

### Law

  * [Item24869](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24869)
    - Content: 20. Authorize negotiation and execution of a legal services agreement with Webb & Webb Attorneys at Law, for legal representation, advice, and counsel relating to the administrative appeal to Texas Commission on Environmental Quality of City of Austin wholesale water rates, for a total contract amount not to exceed $497,000.
  * [Item25127](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25127)
    - Content: 21. Authorize negotiation and execution of an amendment to a legal services contract with Scott Douglas & McConnico for services related to Chapter 245 of the Texas Local Government Code in the amount of $65,000, for a total contract amount not to exceed $121,000.

### Office of Real Estate Services

  * [Item24524](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24524)
    - Content: 22. Authorize the negotiation and execution of an agreement with HILL COUNTRY CONSERVANCY regarding cooperation on the acquisition of conservation easements, including cost sharing that may include reimbursement of Hill Country Conservancy's costs in connection with acquisition in the amount not to exceed $50,000.
  * [Item24567](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24567)
    - Content: 23. Authorize the negotiation and execution of a cooperative agreement with the UNITED STATES OF AMERICA COMMODITY CREDIT CORPORATION and HILL COUNTRY CONSERVANCY allowing the City to participate in the Farm and Ranch Lands Protection Program to continue the City s role in preserving land and protecting water quality in conjunction with the November 6, 2012 Bond Election, Proposition 13.

### Parks and Recreation

  * [Item24585](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24585)
    - Content: 24. Authorize negotiation and execution of an amendment to an interlocal agreement between the City and the Lower Colorado River Authority for buoy installation and maintenance on Lake Austin, Lady Bird Lake, Decker Lake and Lake Travis.
  * [Item24318](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24318)
    - Content: 25. Approve an ordinance authorizing negotiation and execution of a Parkland Improvement Agreement with the Austin Parks Foundation and C3 Presents, L.L.C.to make improvements to the grounds of Auditorium Shores at Town Lake Metropolitan Park, and to participate in an event impact analysis related to this park; authorizing acceptance of a $3,500,000 donation from C3 Presents L.L.C.; amending the Fiscal Year 2012-2013 Parks and Recreation Department Operating Budget Special Revenue Fund (Ordinance No. 20120910-001) to appropriate $3,500,000; and amending the Fiscal Year 2012-2013 Parks and Recreation Department Capital Budget (Ordinance No. 20120910-002) to transfer in and appropriate $3,500,000 from the Parks and Recreation Department Operating Budget Special Revenue Fund for the Auditorium Shores improvements project located within Town Lake Metropolitan Park. Related to Items #12 and #26.
  * [Item24444](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24444)
    - Content: 26. Approve an ordinance amending Chapter 3-4 of the City Code relating to off-leash dog areas to move the portion available at Auditorium Shores. Related to Items #12 and #25.

### Planning and Development Review

  * [Item24500](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24500)
    - Content: 27. Authorize negotiation and execution of a 12-month interlocal agreement with the University of Texas School of Architecture (UT) to jointly fund a summer student internship at the City's Planning & Development Review Department with a cost to the City not to exceed $3,500 and contribution from UT's Kent S. Butler Memorial Excellence Fund in an amount not to exceed $3,500, for a total contract amount not to exceed $7,000. Related to Item #13.
  * [Item24506](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24506)
    - Content: 28. Approve an ordinance amending Ordinance No. 20130214-023, to correct Exhibit H to the Amended and Restated Strategic Partnership Agreement between the City and Lost Creek Municipal Utility District (MUD) to correctly identify MUD property conveyed to the Lost Creek Limited District.

### Public Works

  * [Item24559](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24559)
    - Content: 29. Authorize negotiation and execution of an interlocal agreement between the City of Austin and the Capital Metropolitan Transit Authority for the Redline Trail adjacent to Airport Boulevard from Denson Drive to Lamar Boulevard.
  * [Item24764](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24764)
    - Content: 30. Authorize negotiation and execution of a 12-month interlocal agreement with the University of Texas at Austin for the Green Alley Demonstration Project in the Guadalupe Neighborhood of East Austin in an amount not to exceed $18,000.

### Purchasing Office

  * [Item24765](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24765)
    - Content: 31. Authorize award and execution of a 28-month requirements supply contract through the Texas Local Government Purchasing Cooperative (Buyboard) with STAPLES CONTRACT AND COMMERCIAL, INC. for the purchase of office supplies in an amount not to exceed $6,966,372. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9D (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24769](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24769)
    - Content: 32. Authorize award and execution of a 36-month contract with AUSTIN AGGREGATES, for washed filter sand for the Watershed Protection Department in an estimated amount not to exceed $220,500 with three 12-month extension options in an amount not to exceed $73,500 per extension, for a total contract amount not to exceed $441,000. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9D (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24772](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24772)
    - Content: 33. Authorize award and execution of a 36-month requirements supply contract with PROMEGA CORP., dba PROMEGA, to provide forensics/DNA supplies for the Austin Police Department (APD) in an estimated amount not to exceed $166,518, with three 12-month extension options in an estimated amount not to exceed $55,506 per extension option, for a total estimated contract amount not to exceed $333,036. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9D (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24773](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24773)
    - Content: 34. Authorize negotiation and execution of a contract with CHAMBERLIN ROOFING & WATERPROOFING LLC. for wet glazing and resealing of all exterior windows, trim and granite panels at Town Lake Center, in an amount not to exceed $174,893. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9C (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24774](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24774)
    - Content: 35. Authorize award and execution of a 36-month requirements supply contract with SOVEREIGN MEDICAL, INC., for the purchase of ResQPOD circulatory enhancer devices for the Emergency Medical Services Department in an estimated amount not to exceed $344,075, with three 12-month extension options in an estimated amount not to exceed $138,358 for first extension option, $152,194 for second extension option, and $167,413 for the third extension option, for a total estimated contract amount not to exceed $802,040.
  * [Item24775](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24775)
    - Content: 36. Authorize award and execution of a contract through the Texas Multiple Award Schedule (TXMAS) with ALL BUSINESS MACHINES, INC. for the purchase of a gas chromatograph-headspace analyzer for the Austin Police Department to be used in the identification of narcotic substances in an amount not to exceed $63,463.
  * [Item24776](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24776)
    - Content: 37. Authorize award and execution of a 24-month requirements supply contract with TEXAS LEHIGH CEMENT COMPANY LP, to provide Type 1 Hydraulic Cement for the Public Works Department, in an estimated amount not to exceed $350,000, with two 12-month extension options in estimated amounts not to exceed $175,000 for each extension option, for a total estimated contract amount not to exceed $700,000.
  * [Item24777](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24777)
    - Content: 38. Authorize award and execution of a 36-month requirements supply contract with BAYSCAN TECHNOLOGIES, to provide barcode labels and ribbons for the Austin Police Department in an estimated amount not to exceed $34,327, with two 12-month extension options in an estimated amount not to exceed $12,605 for the first extension option and $13,235 for the second extension option, for a total estimated contract amount not to exceed $60,167.
  * [Item24778](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24778)
    - Content: 39. Authorize award and execution of a 36-month requirements supply contract with CASCO INDUSTRIES INC., to provide aqueous film forming foam for the Austin Fire Department (AFD) in an estimated amount not to exceed $65,151, with three 12-month extension options in an estimated amount not to exceed $21,717 per extension option, for a total estimated contract amount not to exceed $130,302.
  * [Item24779](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24779)
    - Content: 40. Authorize award and execution of a 36-month requirements supply contract with VIDACARE CORPORATION, for the purchase of the EZ-IO intraosseious infusion system and supplies for the Emergency Medical Services Department (EMS) in an estimated amount not to exceed $690,474, with three 12-month extension options in an estimated amount not to exceed $230,158 per extension option, for a total estimated contract amount not to exceed $1,380,948.
  * [Item24780](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24780)
    - Content: 41. Authorize negotiation and execution of a 12-month requirements service contract with CIRRO ENERGY SERVICES, INC., to provide peak load forecasting for Austin Energy in an amount not to exceed $28,000, with four 12-month extension options not to exceed $28,000 per extension option, for a total estimated contract amount not to exceed $140,000. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9C (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24771](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24771)
    - Content: 42. Authorize negotiation and execution of a 24-month requirements service contract with SIEMENS INDUSTRY, INC., for wooden utility pole inspection services for Austin Energy in an amount not to exceed $517,440, with three 12-month extension options in an amount not to exceed $258,720 per extension option, for a total contract amount not to exceed $1,293,600. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9C (Minority-Owned and Women-Owned Business Enterprise Procurement Program) No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )

### Transportation

  * [Item23910](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item23910)
    - Content: 43. Approve a resolution authorizing the negotiation and execution of Amendment No. 1 to the Advance Funding Agreement in the amount of $700,000 between the City and the Texas Department of Transportation for intersection improvements to the IH 35 Northbound Frontage Road from 53rd Street to Barbara Jordan Boulevard.
  * [Item25030](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25030)
    - Content: 44. Approve an ordinance amending City Code Chapter 13-2 (Ground Transportation Passenger Services) to revise the definitions, amend the regulations, and add requirements related to the operation of charter services.
  * [Item25031](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25031)
    - Content: 45. Approve an ordinance amending City Code Chapter 13-2 relating to ground transportation passenger services.

### Item(s) from Council

  * [Item24906](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24906)
    - Content: 46. Approve appointments and certain related waivers to citizen boards and commissions, to Council subcommittees and other intergovernmental bodies and removal and replacement of members.
  * [Item25103](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25103)
    - Content: 47. Approve a resolution directing the City Manager to develop amendments to the City personnel policies to provide 30 days of paid parental leave to be used during Family and Medical Leave Act leave after all accrued leave is exhausted, and directing the City Manager to develop a paid leave bank for employees requiring leave for other family and personal health reasons. ( Notes: SPONSOR: Council Member William Spelman CO 1: Mayor Lee Leffingwell CO 2: Council Member Mike Martinez )
  * [Item25105](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25105)
    - Content: 48. Approve a resolution asking the City Manager to consider installing parking meters in the parking lot that serves the Butler Shores Softball Fields and surrounding streets. ( Notes: SPONSOR: Council Member Chris Riley CO 1: Council Member Mike Martinez CO 2: Mayor Pro Tem Sheryl Cole )
  * [Item25106](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25106)
    - Content: 49. Approve a resolution directing the City Manager to initiate code amendments to create enhanced penalties and a rental registration program applicable to owners of residential rental property that repeatedly violate public health and safety ordinances; and supporting the Building and Standards Commission's and City prosecutor's consideration of repeat offenses when considering penalties for public health and safety violations. ( Notes: SPONSOR: Council Member William Spelman CO 1: Mayor Pro Tem Sheryl Cole )
  * [Item25107](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25107)
    - Content: 50. Approve a resolution directing the City Manager to develop a one-year pilot registration program for residential rental properties located in certain neighborhoods and to explore the feasibility of a code amendment for enhanced fines after the first conviction of a city code violation related to property maintenance. ( Notes: SPONSOR: Council Member Kathie Tovo CO 1: Council Member Mike Martinez )
  * [Item25129](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25129)
    - Content: 51. Approve a resolution initiating a Code amendment related to affordable housing requirements associated with development bonuses under the Planned Unit Development zoning district and directing the City Manager to process the amendment. ( Notes: SPONSOR: Council Member Mike Martinez CO 1: Mayor Lee Leffingwell CO 2: Mayor Pro Tem Sheryl Cole )

### Item(s) to Set Public Hearing(s)

  * [Item24422](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24422)
    - Content: 52. Set a public hearing to receive citizen comments on the City s draft Fiscal Year 2013-2014 Action Plan as required by the U.S. Department of Housing and Urban Development, and the Community Development 2013-2014 Program as required by Texas Local Government Code Chapter 373. (Suggested date and time: June 20, 2013, 4:00 p.m., at Austin City Hall, 301 W. Second Street, Austin, TX).
  * [Item24577](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24577)
    - Content: 53. Set a public hearing to consider the proposed assessments on the Public Improvement District for the Estancia Hill Country project (approximately 600 acres in southern Travis County west of IH 35 South approximately eight tenths of a mile south of the intersection of IH 35 South and Onion Creek Parkway). Suggested date and time: June 20, 2013 at 4:00 pm, Council Chambers of City Hall, 301 W 2nd Street, Austin, Texas. Related to Item #54.

### Action on Item(s) with Closed Public Hearings - per City Code Section 2-5-27, additional speakers will not be registered

  * [Item24582](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24582)
    - Content: 54. Approve a resolution authorizing creation of a Public Improvement District for the Estancia Hill Country project (approximately 600 acres in southern Travis County west of IH 35 South approximately eight tenths of a mile south of the intersection of IH 35 South and Onion Creek Parkway); and authorize negotiation and execution of an agreement relating to financing certain improvements. Related to Item #53.
  * [Item25116](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25116)
    - Content: 55. Approve second and third readings of an ordinance repealing and replacing Article 11 of City Code Chapter 25-12 to adopt the 2012 International Residential Code and local amendments. THE PUBLIC HEARING FOR THIS ITEM WAS HELD AND CLOSED ON MAY 23, 2013.
  * [Item25117](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25117)
    - Content: 56. Approve second and third readings of an ordinance amending Sections 25-12-113 and 25-12-114 relating to requirements of the Electrical Code for registration, inspection, supervision, and suspension. THE PUBLIC HEARING FOR THIS ITEM WAS HELD AND CLOSED ON MAY 23, 2013.

### Morning Briefings

  * [Item24907](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24907)
    - Content: 57. Briefing on the Waller Creek design plan and proposed joint development agreement.
  * [Item24908](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24908)
    - Content: 58. Public Works and Transportation Departments briefing regarding the Rainey Street recommendations.

### Executive Session

  * [Item24673](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24673)
    - Content: 59. Discuss legal issues related to Open Government matters (Private consultation with legal counsel - Section 551.071).
  * [Item24674](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24674)
    - Content: 60. Discuss legal issues related to the transition to electing the council from single-member districts (Private consultation with legal counsel - Section 551.071).
  * [Item25064](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25064)
    - Content: 61. Discuss legal issues related to City of Austin 2013 labor negotiations with employees in the Fire, Police, and EMS departments (Private consultation with legal counsel-Section 551.071).

### Zoning Ordinances / Restrictive Covenants (HEARINGS CLOSED)

  * [Item25032](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25032)
    - Content: 62. C14-2011-0141 - Peaceful Hill Condominiums - Approve third reading of an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 8107 Peaceful Hill Lane and 501 Hubach Lane (South Boggy Creek Watershed) from development reserve (DR) district zoning to townhouse and condominium residence-conditional overlay (SF-6-CO) combining district zoning, with conditions. First reading approved with conditions on April 26, 2012. Vote: 5-2, Mayor Lee Leffingwell and Council Member Tovo voted nay. Second Reading approved with conditions on June 14, 2012. Vote: 5-2 Council Members Morrison and Tovo voted nay. Owner/Applicant: Kristopher Alsworth, Delton Hubach, Jim Bula and Catherine Christopherson. Agent: The Weichert Law Firm (Glenn K. Weichert). City Staff: Wendy Rhoades, 974-7719.
  * [Item25109](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25109)
    - Content: 63. C14-2012-0083 Cirrus Rezoning Approve third reading of an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 800 West 6th Street and 602-702 West Avenue (Shoal Creek Watershed) from downtown mixed use-conditional overlay-central urban redevelopment (DMU-CO-CURE) combining district zoning for Tract 1 and limited office (LO) district zoning and general office (GO) district zoning for Tract 2 to downtown mixed use-central urban redevelopment (DMU-CURE) combining district zoning for Tract 1 and downtown mixed use-conditional overlay (DMU-CO) combining district zoning for Tract 2. First reading approved on April 25, 2013. Vote 5-1. Council Member Tovo voted nay. Council Member Martinez was absent. Second reading approved on May 23, 2013. Vote 6-0. Mayor Leffingwell was off the dais. Applicant: Cirrus Logic, Inc. (Thurman Case). Agent: Armbrust & Brown, PLLC (Richard Suttle). City Staff: Clark Patterson, 974-7691.

### Zoning and Neighborhood Plan Amendments (Public Hearings and Possible Action)

  * [Item24418](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24418)
    - Content: 64. NPA-2012-0023.01 - 1504 East 51st Street (Promiseland) - Conduct a public hearing and approve an ordinance amending Ordinance No. 20070809-55 of the University Hills/Windsor Park Combined Neighborhood Plan, to change the future land use designation on the future land use map (FLUM) on property locally known as 1504 East 51st Street (Tannehill Branch Watershed) from Civic to Mixed Use land use. Staff Recommendation: To grant Mixed Use land use. Planning Commission Recommendation: To grant Mixed Use land use and Open Space land use. Owner: The World of Pentecost Inc./HCM, LLC (Shules Hersh). Applicant and Agent: Hughes Capital Management, Inc. (Trac Bledsoe). City Staff: Maureen Meredith, 974-2695.
  * [Item24648](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24648)
    - Content: 65. NPA-2012-0016.01.SH - 1141 Shady Lane (thinkEAST Austin) - Conduct a public hearing and approve an ordinance amending Ordinance No. 20030327-12 of the Govalle/Johnston Terrace Combined Neighborhood Plan, an element of the Imagine Austin Comprehensive Plan, to change the future land use designation on the future land use map (FLUM) on property locally known as 1411 Shady Lane and 5300 Jain Lane (Boggy Creek Watershed) from Single Family land use and Mixed Use land use to Major Planned Developments land use. Staff Recommendation: To grant Major Planned Developments land use. Planning Commission Recommendation: To grant Major Planned Development land use. Owner: thinkEAST Austin, L.P. Applicant/Agent: thinkEAST Austin Management, L.L.C. (Richard de Varga). City Staff: Maureen Meredith, 974-2695.
  * [Item24649](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24649)
    - Content: 66. C814-2012-0128.SH thinkEAST Austin- Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 1411 Shady Lane and 5300 Jain Lane (Boggy Creek Watershed; Tannehill Branch Watershed) from limited office-mixed use-conditional overlay-neighborhood plan (LO-MU-CO-NP) combining district and family residence-neighborhood plan (SF-3-NP) combining district zoning to planned unit development-neighborhood plan (PUD-NP) combining district zoning. Staff Recommendation: To grant planned unit development-neighborhood plan (PUD-NP) combining district zoning, with conditions. Planning Commission Recommendation: To grant planned unit development-neighborhood plan (PUD-NP) combining district zoning, with conditions.Owner/Agent: thinkEAST Austin, LP (Richard deVarga). City Staff: Heather Chaffin, 974-2122.
  * [Item25056](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25056)
    - Content: 67. NPA-2013-0011.01 4805 Harmon Avenue Conduct a public hearing and approve an ordinance amending Ordinance No. 020523-30, the North Loop Neighborhood Plan, an element of the Imagine Austin Comprehensive Plan, to change the land use designation on the future land use map (FLUM) on property locally known as 4805 Harmon Avenue (Tannehill Branch Watershed) from Commercial land use to Single Family land use. Staff Recommendation: To grant Single Family land use. Planning Commission Recommendation: To grant Single Family land use. Owner/Applicant: Mackey Adams Properties, Inc. (Harry L. Mackey). Agent: Karen Radtke Interior Design (Karen Radtke). City Staff: Maureen Meredith, 974-2695.
  * [Item25051](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25051)
    - Content: 68. C14-2013-0013 4805 Harmon Ave. Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 4805 Harmon Avenue (Boggy Creek Watershed) from limited office-conditional overlay-neighborhood plan (LO-CO-NP) combining district zoning to single family residence-small lot-neighborhood plan (SF-4A-NP) combining district zoning. Staff Recommendation: To grant single family residence-small lot-conditional overlay- neighborhood plan (SF-4A-CO-NP) combining district zoning. Planning Commission Recommendation: To grant single family residence-small lot-conditional overlay-neighborhood plan (SF-4A-CO-NP) combining district zoning. Applicant: Mackey Adams Properties, Inc. (Harry Mackey). Agent: Karen Radtke Interior Design (Karen Radtke). City Staff: Clark Patterson, 974-7691.
  * [Item25057](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25057)
    - Content: 69. NPA-2013-0011.02 4914 Bennett Avenue (Tomlinson s Feed & Pets, Inc.) Conduct a public hearing and approve an ordinance amending Ordinance No. 020523-30, the North Loop Neighborhood Plan, an element of the Imagine Austin Comprehensive Plan, to change the land use designation on the future land use map (FLUM) on property locally known as 4914 Bennett Avenue (Tannehill Branch Watershed) from Single Family land use to Mixed Use land use. Staff Recommendation: To grant Mixed Use/Office land use. Planning Commission Recommendation: To grant indefinite postponement by Applicant. Owner/Applicant: Tomlinson s Feed & Pets, Inc. (Scott Click). Agent: Thrower Design (A. Ron Thrower). City Staff: Maureen Meredith, 974-2695.
  * [Item25050](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25050)
    - Content: 70. C14-2013-0021 Tomlinson s Feed & Pets Rezoning Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 4914 Bennett Avenue (Boggy Creek Watershed) from family residence-neighborhood plan (SF-3-NP) combining district zoning to community commercial-mixed use-neighborhood plan (GR-MU-NP) combining district zoning. Staff Recommendation: To grant community commercial-mixed use-conditional overlay-neighborhood plan (GR-MU-CO-NP) combining district zoning. Planning Commission Recommendation: To grant indefinite postponement request by Applicant. Applicant: Tomlinson s Feed & Pets, Inc. (Scott Click). Agent: Thrower Design (Ron Thrower). City Staff: Clark Patterson, 974-7691.
  * [Item25055](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25055)
    - Content: 71. NPA-2013-0025.04 6110 Hill Forest Drive (Beiter-2) Conduct a public hearing and approve an ordinance amending Ordinance No. 20081211-096, the Oak Hill Combined Neighborhood Plan, an element of the Imagine Austin Comprehensive Plan, to change the land use designation on the future land use map (FLUM) on property locally known as 6110 Hill Forest Drive (Williamson Creek Watershed-Barton Springs Zone) from Single Family land use to Higher Density Single Family land use. Staff Recommendation: To grant Higher Density Single Family land use. Planning Commission Recommendation: To grant Higher Density Single Family land use. Owner: Michael and Paulette Beiter. Applicant/Agent: Jim Bennett Consulting (Jim Bennett). City Staff: Maureen Meredith, 974-2695.
  * [Item25059](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25059)
    - Content: 72. C14-2013-0018 Beiter-2 Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 6110 Hill Forest Drive (Williamson Creek Watershed-Barton Springs Zone) from family residence-neighborhood plan (SF-3-NP) combining district zoning to urban family residence-conditional overlay-neighborhood plan (SF-5-CO-NP) combining district zoning. Staff Recommendation: To grant urban family residence-conditional overlay-neighborhood plan (SF-5-CO-NP) combining district zoning. Planning Commission Recommendation: To grant urban family residence-conditional overlay-neighborhood plan (SF-5-CO-NP) combining district zoning, with conditions. Owner/Applicant: Michael and Paulette Beiter. Agent: Jim Bennett Consulting (Jim Bennett). City Staff: Wendy Rhoades, 974-7719.
  * [Item25033](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25033)
    - Content: 73. C14-2012-0140 - Street and Bridge District Office- Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 3511 Manor Road (Tannehill Branch Watershed) from community commercial-neighborhood plan (GR-NP) combining district zoning and community commercial-vertical mixed use building-neighborhood plan (GR-V-NP) combining district zoning to general commercial services-conditional overlay-neighborhood plan (CS-CO-NP) combining district zoning. Staff Recommendation: To grant general commercial services-conditional overlay-neighborhood plan (CS-CO-NP) combining district zoning. Planning Commission Recommendation: To be reviewed on June 25, 2013. Owner/Agent: City of Austin (Peter Davis). City Staff: Heather Chaffin, 974-2122. A valid petition has been filed in opposition to this rezoning request.
  * [Item25034](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25034)
    - Content: 74. C814-2012-0160 - 211 South Lamar Boulevard Planned Unit Development - Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 211 South Lamar Boulevard (Lady Bird Lake Watershed) from general commercial services (CS) district zoning and general commercial services-vertical mixed use building (CS-V) combining district zoning to planned unit development (PUD) district zoning. Staff Recommendation: To grant planned unit development (PUD) district zoning. Planning Commission Recommendation: To be reviewed June 11, 2013. Owner: Post Paggi, LLC (Jason Post). Applicant: Winstead PC (Stephen O. Drenner). City Staff: Lee Heckman, 974-7604.
  * [Item24650](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24650)
    - Content: 75. C14-2013-0011 - Restaurant - Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by zoning property locally known as 11800 Arabian Trail (Walnut Creek Watershed) from single family residence-standard lot (SF-2) district zoning to community commercial-conditional overlay (GR-CO) combining district zoning. Staff Recommendation: To deny community commercial-conditional overlay (GR-CO) combining district zoning. Zoning and Platting Commission Recommendation: To be reviewed on June 4, 2013. Owner/Applicant: Xue Yun Tang. Agent: Shaw Hamilton Consultants (Shaw Hamilton). City Staff: Sherri Sirwaitis, 974-3057.
  * [Item25035](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25035)
    - Content: 76. C14-2013-0020 - Oak Creek Village - Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 2324 Wilson Street (Bouldin Creek Watershed) from multi-family residence medium density-neighborhood plan (MF-3-NP) combining district zoning to multi-family residence-highest density-conditional overlay-neighborhood plan (MF-6-CO-NP) combining district zoning. Staff Recommendation: To grant multi-family residence highest density-conditional overlay-neighborhood plan (MF-6-CO-NP) combining district zoning with conditions. Planning Commission Recommendation: To grant multi-family residence-highest density-conditional overlay-neighborhood plan (MF-6-CO-NP) combining district zoning with conditions. Owner: 2007 Travis Heights, LP (Rene D. Campos). Applicant/Agent: Winstead PC (John Donisi). City Staff: Lee Heckman, 974-7604.
  * [Item25062](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25062)
    - Content: 77. C14-2013-0031 Clawson Patio Homes Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 3907 Clawson Road (West Bouldin Creek Watershed) from family residence (SF-3) district zoning to urban family residence (SF-5) district zoning. Staff Recommendation: To grant urban family residence-conditional overlay (SF-5-CO) combining district zoning with conditions. Planning Commission Recommendation: To be reviewed on June 25, 2013. Owner: Dean Chen. Applicant: Bleyl Interests, Inc. (Vincent G. Huebinger). City Staff: Lee Heckman, 974-7604.
  * [Item25063](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25063)
    - Content: 78. C14-2013-0032 Clawson Patio Homes II Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 3903 Clawson Road (West Bouldin Creek Watershed) from family residence (SF-3) district zoning to multi-family residence-limited density (MF-1) district zoning. Staff Recommendation: To grant multi-family residence-limited density-conditional overlay (MF-1-CO) combining district zoning with conditions. Planning Commission Recommendation: To be reviewed on June 25, 2013. Owner: Roy G. Crouse. Applicant: Bleyl Interests, Inc. (Vincent G. Huebinger). City Staff: Lee Heckman, 974-7604.
  * [Item25061](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25061)
    - Content: 79. C14-2013-0037 7509 Manchaca Office Park Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 7509 Manchaca Road (Williamson Creek Watershed) from warehouse/limited office-conditional overlay (W/LO-CO) combining district zoning to limited office (LO) district zoning. Staff Recommendation: To grant limited office-conditional overlay (LO-CO) combining district zoning. Zoning and Platting Commission Recommendation: To grant limited office-conditional overlay (LO-CO) combining district zoning. Owner/Applicant: 7509 Manchaca, LLC (Mervin Fatter). Agent: Fatter & Evans, Architect (Mervin Fatter). City Staff: Wendy Rhoades, 974-7719.
  * [Item25054](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25054)
    - Content: 80. C14-2013-0041 Fort Dessau-GR Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 1602 Fish Lane (Harris Branch Watershed) from single family residence-standard lot-conditional overlay (SF-2-CO) combining district zoning to multi-family residence-medium density (MF-3) district zoning. Staff Recommendation: To grant multi-family residence-medium density (MF-3) district zoning. Zoning and Platting Commission Recommendation: To be reviewed on June 18, 2013. Owner/Applicant: John C. & Dana Fish. Agent: Land Strategies, Inc. (Paul W. Linehan). City Staff: Sherri Sirwaitis, 974-3057.
  * [Item25053](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25053)
    - Content: 81. C14-2013-0042 Fort Dessau-P Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 13826 Dessau Road (Harris Branch Watershed) from single family residence-standard lot-conditional overlay (SF-2-CO) combining district zoning to public (P) district zoning. Staff Recommendation: To grant public (P) district zoning. Zoning and Platting Commission Recommendation: To grant public (P) district zoning. Owner/Applicant: John C. & Dana Fish. Agent: Land Strategies, Inc. (Paul W. Linehan). City Staff: Sherri Sirwaitis, 974-3057.
  * [Item25052](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25052)
    - Content: 82. C14-2013-0043 Fort Dessau-SF-6 Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 13826 Dessau Road (Harris Branch Watershed) from neighborhood commercial (LR) district zoning to townhouse & condominium residence (SF-6) district zoning. Staff Recommendation: To grant townhouse & condominium residence (SF-6) district zoning. Zoning and Platting Commission Recommendation: To grant townhouse & condominium residence (SF-6) district zoning. Owner/Applicant: John C. & Dana Fish. Agent: Land Strategies, Inc. (Paul W. Linehan). City Staff: Sherri Sirwaitis, 974-3057.

### Austin Housing and Finance Corporation Meeting

  * [Item25098](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25098)
    - Content: 83. The Mayor will recess the City Council meeting to conduct a Board of Directors' Meeting of the Austin Housing Finance Corporation. Following adjournment of the AHFC Board meeting the City Council will reconvene. (The AHFC agenda is temporarily located at https://austin.siretechnologies.com/sirepub/mtgviewer.aspx?meetid=420&doctype=AGENDA).

### Public Hearings and Possible Actions

  * [Item24407](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24407)
    - Content: 84. Conduct a public hearing and consider an ordinance amending City Code Chapter 25-2 (Zoning) to create the Central Austin - University Area Zoning Overlay District in which a group residential land use is a conditional use in the multi-family residence moderate-high density (MF-4) base zoning district.
  * [Item24424](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24424)
    - Content: 85. Conduct a public hearing and consider an ordinance amending City Code Chapter 25-2, Subchapter C, Article 3, Division 9 (University Neighborhood Overlay District Requirements) relating to affordable housing regulations in the university neighborhood overlay (UNO) district.
  * [Item24458](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24458)
    - Content: 86. Conduct a public hearing and consider an ordinance amending City Code Chapters 8-1 and 25-6 to authorize parking utilization agreements for certain under-used parking lots on city parkland, in exchange for funding to provide significant amenities or improvements to serve the park.
  * [Item24501](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item24501)
    - Content: 87. Conduct a public hearing and consider an ordinance amending City Code Chapter 25-2 relating to the use classifications of electronic prototype assembly and electronic testing in the downtown mixed use and central business district base zoning districts.
  * [Item25039](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25039)
    - Content: 88. Conduct a public hearing and consider an ordinance amending City Code Chapter 25-2, Subchapter E, relating to design standards and mixed use for development projects.
  * [Item25041](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25041)
    - Content: 89. Conduct a public hearing and consider an ordinance repealing and replacing Article 1 of City Code Chapter 25-12 to adopt the 2012 International Building Code and local amendments.
  * [Item25042](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25042)
    - Content: 90. Conduct a public hearing and consider an ordinance repealing and replacing Article 5 of City Code Chapter 25-12 to adopt the 2012 Uniform Mechanical Code and local amendments.
  * [Item25043](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25043)
    - Content: 91. Conduct a public hearing and consider an ordinance repealing and replacing Article 12 of City Code Chapter 25-12 to adopt the 2012 International Energy Conservation Code and local amendments.
  * [Item25044](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25044)
    - Content: 92. Conduct a public hearing and consider an ordinance repealing and replacing City Code Chapter 25-12, Article 7 to adopt the 2012 International Fire Code and local amendments.
  * [Item25049](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=423&agviewdoctype=AGENDA#Item25049)
    - Content: 93. Conduct a public hearing and consider an ordinance repealing and replacing Article 6 of City Code Chapter 25-12 to adopt the 2012 Uniform Plumbing Code and local amendments.


# [Meeting 430](http://austin.siretechnologies.com/sirepub/mtgviewer.aspx?meetid=430&amp;doctype=AGENDA)

## [Agenda 2013-06-06](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=430&agviewdoctype=AGENDA)

  No categorized agenda items found


# [Meeting 403](http://austin.siretechnologies.com/sirepub/mtgviewer.aspx?meetid=403&amp;doctype=agenda)

## [Agenda 2013-06-06](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA)


### Father Bill Wack, CSC, Pastor, St. Ignatius Catholic Church

  * [Item23567](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item23567)
    - Content: 10:00 AM City Council Convenes

### Austin Energy

  * [Item24882](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24882)
    - Content: 2. Authorize negotiation and execution of an agreement with Seton Healthcare, to provide a performance-based incentive for the generation of solar energy at its facility located at 4900 Mueller Blvd., Austin, Texas 78723, over a 10-year period for an estimated $7,325 per year, for a total amount not to exceed $73,250.
    - Zip code: [78723](https://maps.google.com/maps?q=78723)
  * [Item24883](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24883)
    - Content: 3. Authorize negotiation and execution of an agreement with St. David s Episcopal Church, to provide a performance-based incentive for the generation of solar energy at its facility located at 308 E. 8th St., Austin, Texas 78701, over a 10-year period for an estimated $26,969 per year, for a total amount not to exceed $269,690.
    - Zip code: [78701](https://maps.google.com/maps?q=78701)
  * [Item24884](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24884)
    - Content: 4. Approve issuance of a rebate to Arts Apartments South Austin, LLC, for performing comprehensive energy efficiency upgrades at the Arts Apartments at South Austin located at 400 West St. Elmo Rd., Austin, Texas 78745, in an amount not to exceed $99,000.
    - Zip code: [78745](https://maps.google.com/maps?q=78745)
  * [Item24886](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24886)
    - Content: 5. Approve issuance of a rebate to Arts Apartments at Turtle Creek, LLC, for performing comprehensive energy efficiency upgrades at the Arts Apartments at the Turtle Creek located at 714 Turtle Creek Blvd., Austin, Texas 78745, in an amount not to exceed $71,500.
    - Zip code: [78745](https://maps.google.com/maps?q=78745)
  * [Item24887](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24887)
    - Content: 6. Approve issuance of a rebate to H&H Texas Partners, LTD, for performing comprehensive energy efficiency upgrades at the Timbercreek Apartments located at 614 South 1st St., Austin, Texas 78704, in an amount not to exceed $132,000.
    - Zip code: [78704](https://maps.google.com/maps?q=78704)
  * [Item24888](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24888)
    - Content: 7. Approve issuance of a rebate to Capri Capital for performing duct diagnostics and improvements at the Stonegate Apartments located at 10505 South IH-35, Austin, Texas 78747, in an amount not to exceed $146,974.
    - Zip code: [78747](https://maps.google.com/maps?q=78747)

### Austin Water Utility

  * [Item25047](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25047)
    - Content: 8. Approve Service Extension Request No. 3151 for water service to a 37 acre tract at 9300 W SH 71, located within the Drinking Water Protection Zone and partially in the City's 2-mile extraterritorial jurisdiction. Related to Item #9.
  * [Item25048](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25048)
    - Content: 9. Approve Service Extension Request No. 3152 for wastewater service to a 37 acre tract at 9300 W SH 71, located within the Drinking Water Protection Zone and partially in the City's 2-mile extraterritorial jurisdiction. Related to Item #8.

### Aviation

  * [Item24412](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24412)
    - Content: 10. Authorize negotiation and execution of an amendment to the lease and hangar facility development agreement with Ascend AUS, LLC to require the Department of Aviation to relocate a Federal Aviation Administration signal cable, refund tenant a total amount of $173,804.00, and modify other lease terms consistent with the mediated settlement agreement dated July 17, 2012.

### Contract Management

  * [Item24564](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24564)
    - Content: 11. Authorize execution of change order #3 to the construction contract with DENUCCI CONSTRUCTORS, for the Pedernales Street Reconstruction and Utility Adjustment from 6th Street to Webberville Road (Group 7) Project, an ACCELERATE AUSTIN PROJECT, in the amount of $65,000 plus additional contingency in the amount of $88,459.90, for a total contract amount not to exceed $2,066,117.80. ( Notes: This contract was awarded in compliance with City Code Chapter 2-9A (Minority Owned and Women Owned Business Enterprise Procurement Program) with 4.45% MBE and 1.54% WBE subcontractor participation to date. )
  * [Item24135](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24135)
    - Content: 12. Authorize negotiation and execution of an amendment to the professional services agreement with THE BROUSSARD GROUP, INC., DBA TBG PARTNERS, A TEXAS CORPORATION, for architectural and engineering services for the Town Lake Metropolitan Park- Auditorium Shores event lawn, off-leash area and central lawn area of Town Lake Park north of Riverside Drive in the amount of $400,000, for a total contract amount not to exceed $2,982,163.35. Related to Items #25 and #26.

### Economic Growth and Redevelopment Services

  * [Item24628](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24628)
    - Content: 13. Authorize the negotiation and execution of Amendment No. 1 to the Interlocal Agreement for Services to Develop an Analytic Tool for Sustainable Communities Regional Planning with the Capital Area Council of Governments to increase the amount payable to the City by $12,500 to employ summer interns, for a total contract amount not to exceed $205,507. Related to Item #27.

### Emergency Medical Services

  * [Item25027](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25027)
    - Content: 14. Authorize negotiation and execution of an amendment to the existing interlocal cooperation agreement between the City and Travis County to provide additional emergency medical services in an area of Travis County outside the City's corporate limits for a period beginning June 1, 2013 through September 30, 2013 in exchange for payment of $188,838 by Travis County. Related to Item #15.
  * [Item25028](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25028)
    - Content: 15. Approve an ordinance amending the Fiscal Year 2012-2013 General Fund Emergency Medical Services Department Operating Budget (Ordinance No. 20120910-001) to increase revenue in the amount of $188,838 and increase expenses in the amount of $188,838 to add six new EMS Medic I full-time equivalent positions. Related to Item #14.
  * [Item25029](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25029)
    - Content: 16. Approve an ordinance establishing classifications and positions in the classified service of the Emergency Medical Services Department; eliminating and creating certain positions in the classified service of the Emergency Medical Services Department; and repealing ordinance no. 20130328-015 relating to Emergency Medical Services Department classifications and positions.

### Government Relations

  * [Item24641](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24641)
    - Content: 17. Approve a resolution adopting the City's federal legislative program for the 113th Congress, including Fiscal Year 2014 appropriations requests.

### Health and Human Services

  * [Item24561](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24561)
    - Content: 18. Authorize negotiation and execution of a social services contract with SKILLPOINT ALLIANCE, for a twelve-month term beginning on July 1, 2013 and ending on June 30, 2014, in an amount not to exceed $150,000, with no extension options.
  * [Item24566](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24566)
    - Content: 19. Authorize negotiation and execution of an interlocal agreement with THE UNIVERSITY OF TEXAS HEALTH SCIENCE CENTER AT HOUSTON for a 12 month term from July 1, 2013 through June 30, 2014 for evaluation of a healthy-based initiative to address the needs of economically disadvantaged households in Austin in a total amount not to exceed $50,000.

### Law

  * [Item24869](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24869)
    - Content: 20. Authorize negotiation and execution of a legal services agreement with Webb & Webb Attorneys at Law, for legal representation, advice, and counsel relating to the administrative appeal to Texas Commission on Environmental Quality of City of Austin wholesale water rates, for a total contract amount not to exceed $497,000.
  * [Item25127](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25127)
    - Content: 21. Authorize negotiation and execution of an amendment to a legal services contract with Scott Douglas & McConnico for services related to Chapter 245 of the Texas Local Government Code in the amount of $65,000, for a total contract amount not to exceed $121,000.

### Office of Real Estate Services

  * [Item24524](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24524)
    - Content: 22. Authorize the negotiation and execution of an agreement with HILL COUNTRY CONSERVANCY regarding cooperation on the acquisition of conservation easements, including cost sharing that may include reimbursement of Hill Country Conservancy's costs in connection with acquisition in the amount not to exceed $50,000.
  * [Item24567](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24567)
    - Content: 23. Authorize the negotiation and execution of a cooperative agreement with the UNITED STATES OF AMERICA COMMODITY CREDIT CORPORATION and HILL COUNTRY CONSERVANCY allowing the City to participate in the Farm and Ranch Lands Protection Program to continue the City s role in preserving land and protecting water quality in conjunction with the November 6, 2012 Bond Election, Proposition 13.

### Parks and Recreation

  * [Item24585](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24585)
    - Content: 24. Authorize negotiation and execution of an amendment to an interlocal agreement between the City and the Lower Colorado River Authority for buoy installation and maintenance on Lake Austin, Lady Bird Lake, Decker Lake and Lake Travis.
  * [Item24318](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24318)
    - Content: 25. Approve an ordinance authorizing negotiation and execution of a Parkland Improvement Agreement with the Austin Parks Foundation and C3 Presents, L.L.C.to make improvements to the grounds of Auditorium Shores at Town Lake Metropolitan Park, and to participate in an event impact analysis related to this park; authorizing acceptance of a $3,500,000 donation from C3 Presents L.L.C.; amending the Fiscal Year 2012-2013 Parks and Recreation Department Operating Budget Special Revenue Fund (Ordinance No. 20120910-001) to appropriate $3,500,000; and amending the Fiscal Year 2012-2013 Parks and Recreation Department Capital Budget (Ordinance No. 20120910-002) to transfer in and appropriate $3,500,000 from the Parks and Recreation Department Operating Budget Special Revenue Fund for the Auditorium Shores improvements project located within Town Lake Metropolitan Park. Related to Items #12 and #26.
  * [Item24444](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24444)
    - Content: 26. Approve an ordinance amending Chapter 3-4 of the City Code relating to off-leash dog areas to move the portion available at Auditorium Shores. Related to Items #12 and #25.

### Planning and Development Review

  * [Item24500](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24500)
    - Content: 27. Authorize negotiation and execution of a 12-month interlocal agreement with the University of Texas School of Architecture (UT) to jointly fund a summer student internship at the City's Planning & Development Review Department with a cost to the City not to exceed $3,500 and contribution from UT's Kent S. Butler Memorial Excellence Fund in an amount not to exceed $3,500, for a total contract amount not to exceed $7,000. Related to Item #13.
  * [Item24506](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24506)
    - Content: 28. Approve an ordinance amending Ordinance No. 20130214-023, to correct Exhibit H to the Amended and Restated Strategic Partnership Agreement between the City and Lost Creek Municipal Utility District (MUD) to correctly identify MUD property conveyed to the Lost Creek Limited District.

### Public Works

  * [Item24559](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24559)
    - Content: 29. Authorize negotiation and execution of an interlocal agreement between the City of Austin and the Capital Metropolitan Transit Authority for the Redline Trail adjacent to Airport Boulevard from Denson Drive to Lamar Boulevard.
  * [Item24764](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24764)
    - Content: 30. Authorize negotiation and execution of a 12-month interlocal agreement with the University of Texas at Austin for the Green Alley Demonstration Project in the Guadalupe Neighborhood of East Austin in an amount not to exceed $18,000.

### Purchasing Office

  * [Item24765](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24765)
    - Content: 31. Authorize award and execution of a 28-month requirements supply contract through the Texas Local Government Purchasing Cooperative (Buyboard) with STAPLES CONTRACT AND COMMERCIAL, INC. for the purchase of office supplies in an amount not to exceed $6,966,372. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9D (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24769](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24769)
    - Content: 32. Authorize award and execution of a 36-month contract with AUSTIN AGGREGATES, for washed filter sand for the Watershed Protection Department in an estimated amount not to exceed $220,500 with three 12-month extension options in an amount not to exceed $73,500 per extension, for a total contract amount not to exceed $441,000. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9D (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24772](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24772)
    - Content: 33. Authorize award and execution of a 36-month requirements supply contract with PROMEGA CORP., dba PROMEGA, to provide forensics/DNA supplies for the Austin Police Department (APD) in an estimated amount not to exceed $166,518, with three 12-month extension options in an estimated amount not to exceed $55,506 per extension option, for a total estimated contract amount not to exceed $333,036. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9D (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24773](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24773)
    - Content: 34. Authorize negotiation and execution of a contract with CHAMBERLIN ROOFING & WATERPROOFING LLC. for wet glazing and resealing of all exterior windows, trim and granite panels at Town Lake Center, in an amount not to exceed $174,893. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9C (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24774](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24774)
    - Content: 35. Authorize award and execution of a 36-month requirements supply contract with SOVEREIGN MEDICAL, INC., for the purchase of ResQPOD circulatory enhancer devices for the Emergency Medical Services Department in an estimated amount not to exceed $344,075, with three 12-month extension options in an estimated amount not to exceed $138,358 for first extension option, $152,194 for second extension option, and $167,413 for the third extension option, for a total estimated contract amount not to exceed $802,040.
  * [Item24775](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24775)
    - Content: 36. Authorize award and execution of a contract through the Texas Multiple Award Schedule (TXMAS) with ALL BUSINESS MACHINES, INC. for the purchase of a gas chromatograph-headspace analyzer for the Austin Police Department to be used in the identification of narcotic substances in an amount not to exceed $63,463.
  * [Item24776](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24776)
    - Content: 37. Authorize award and execution of a 24-month requirements supply contract with TEXAS LEHIGH CEMENT COMPANY LP, to provide Type 1 Hydraulic Cement for the Public Works Department, in an estimated amount not to exceed $350,000, with two 12-month extension options in estimated amounts not to exceed $175,000 for each extension option, for a total estimated contract amount not to exceed $700,000.
  * [Item24777](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24777)
    - Content: 38. Authorize award and execution of a 36-month requirements supply contract with BAYSCAN TECHNOLOGIES, to provide barcode labels and ribbons for the Austin Police Department in an estimated amount not to exceed $34,327, with two 12-month extension options in an estimated amount not to exceed $12,605 for the first extension option and $13,235 for the second extension option, for a total estimated contract amount not to exceed $60,167.
  * [Item24778](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24778)
    - Content: 39. Authorize award and execution of a 36-month requirements supply contract with CASCO INDUSTRIES INC., to provide aqueous film forming foam for the Austin Fire Department (AFD) in an estimated amount not to exceed $65,151, with three 12-month extension options in an estimated amount not to exceed $21,717 per extension option, for a total estimated contract amount not to exceed $130,302.
  * [Item24779](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24779)
    - Content: 40. Authorize award and execution of a 36-month requirements supply contract with VIDACARE CORPORATION, for the purchase of the EZ-IO intraosseious infusion system and supplies for the Emergency Medical Services Department (EMS) in an estimated amount not to exceed $690,474, with three 12-month extension options in an estimated amount not to exceed $230,158 per extension option, for a total estimated contract amount not to exceed $1,380,948.
  * [Item24780](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24780)
    - Content: 41. Authorize negotiation and execution of a 12-month requirements service contract with CIRRO ENERGY SERVICES, INC., to provide peak load forecasting for Austin Energy in an amount not to exceed $28,000, with four 12-month extension options not to exceed $28,000 per extension option, for a total estimated contract amount not to exceed $140,000. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9C (Minority-Owned and Women-Owned Business Enterprise Procurement Program). No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )
  * [Item24771](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24771)
    - Content: 42. Authorize negotiation and execution of a 24-month requirements service contract with SIEMENS INDUSTRY, INC., for wooden utility pole inspection services for Austin Energy in an amount not to exceed $517,440, with three 12-month extension options in an amount not to exceed $258,720 per extension option, for a total contract amount not to exceed $1,293,600. ( Notes: This contract will be awarded in compliance with City Code Chapter 2-9C (Minority-Owned and Women-Owned Business Enterprise Procurement Program) No subcontracting opportunities were identified; therefore, no goals were established for this solicitation. )

### Transportation

  * [Item23910](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item23910)
    - Content: 43. Approve a resolution authorizing the negotiation and execution of Amendment No. 1 to the Advance Funding Agreement in the amount of $700,000 between the City and the Texas Department of Transportation for intersection improvements to the IH 35 Northbound Frontage Road from 53rd Street to Barbara Jordan Boulevard.
  * [Item25030](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25030)
    - Content: 44. Approve an ordinance amending City Code Chapter 13-2 (Ground Transportation Passenger Services) to revise the definitions, amend the regulations, and add requirements related to the operation of charter services.
  * [Item25031](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25031)
    - Content: 45. Approve an ordinance amending City Code Chapter 13-2 relating to ground transportation passenger services.

### Item(s) from Council

  * [Item24906](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24906)
    - Content: 46. Approve appointments and certain related waivers to citizen boards and commissions, to Council subcommittees and other intergovernmental bodies and removal and replacement of members.
  * [Item25103](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25103)
    - Content: 47. Approve a resolution directing the City Manager to develop amendments to the City personnel policies to provide 30 days of paid parental leave to be used during Family and Medical Leave Act leave after all accrued leave is exhausted, and directing the City Manager to develop a paid leave bank for employees requiring leave for other family and personal health reasons. ( Notes: SPONSOR: Council Member William Spelman CO 1: Mayor Lee Leffingwell CO 2: Council Member Mike Martinez )
  * [Item25105](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25105)
    - Content: 48. Approve a resolution asking the City Manager to consider installing parking meters in the parking lot that serves the Butler Shores Softball Fields and surrounding streets. ( Notes: SPONSOR: Council Member Chris Riley CO 1: Council Member Mike Martinez CO 2: Mayor Pro Tem Sheryl Cole )
  * [Item25106](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25106)
    - Content: 49. Approve a resolution directing the City Manager to initiate code amendments to create enhanced penalties and a rental registration program applicable to owners of residential rental property that repeatedly violate public health and safety ordinances; and supporting the Building and Standards Commission's and City prosecutor's consideration of repeat offenses when considering penalties for public health and safety violations. ( Notes: SPONSOR: Council Member William Spelman CO 1: Mayor Pro Tem Sheryl Cole )
  * [Item25107](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25107)
    - Content: 50. Approve a resolution directing the City Manager to develop a one-year pilot registration program for residential rental properties located in certain neighborhoods and to explore the feasibility of a code amendment for enhanced fines after the first conviction of a city code violation related to property maintenance. ( Notes: SPONSOR: Council Member Kathie Tovo CO 1: Council Member Mike Martinez )
  * [Item25129](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25129)
    - Content: 51. Approve a resolution initiating a Code amendment related to affordable housing requirements associated with development bonuses under the Planned Unit Development zoning district and directing the City Manager to process the amendment. ( Notes: SPONSOR: Council Member Mike Martinez CO 1: Mayor Lee Leffingwell CO 2: Mayor Pro Tem Sheryl Cole )

### Item(s) to Set Public Hearing(s)

  * [Item24422](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24422)
    - Content: 52. Set a public hearing to receive citizen comments on the City s draft Fiscal Year 2013-2014 Action Plan as required by the U.S. Department of Housing and Urban Development, and the Community Development 2013-2014 Program as required by Texas Local Government Code Chapter 373. (Suggested date and time: June 20, 2013, 4:00 p.m., at Austin City Hall, 301 W. Second Street, Austin, TX).
  * [Item24577](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24577)
    - Content: 53. Set a public hearing to consider the proposed assessments on the Public Improvement District for the Estancia Hill Country project (approximately 600 acres in southern Travis County west of IH 35 South approximately eight tenths of a mile south of the intersection of IH 35 South and Onion Creek Parkway). Suggested date and time: June 20, 2013 at 4:00 pm, Council Chambers of City Hall, 301 W 2nd Street, Austin, Texas. Related to Item #54.

### Action on Item(s) with Closed Public Hearings - per City Code Section 2-5-27, additional speakers will not be registered

  * [Item24582](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24582)
    - Content: 54. Approve a resolution authorizing creation of a Public Improvement District for the Estancia Hill Country project (approximately 600 acres in southern Travis County west of IH 35 South approximately eight tenths of a mile south of the intersection of IH 35 South and Onion Creek Parkway); and authorize negotiation and execution of an agreement relating to financing certain improvements. Related to Item #53.
  * [Item25116](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25116)
    - Content: 55. Approve second and third readings of an ordinance repealing and replacing Article 11 of City Code Chapter 25-12 to adopt the 2012 International Residential Code and local amendments. THE PUBLIC HEARING FOR THIS ITEM WAS HELD AND CLOSED ON MAY 23, 2013.
  * [Item25117](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25117)
    - Content: 56. Approve second and third readings of an ordinance amending Sections 25-12-113 and 25-12-114 relating to requirements of the Electrical Code for registration, inspection, supervision, and suspension. THE PUBLIC HEARING FOR THIS ITEM WAS HELD AND CLOSED ON MAY 23, 2013.

### 10:30 AM - Morning Briefings

  * [Item24907](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24907)
    - Content: 57. Briefing on the Waller Creek design plan and proposed joint development agreement.
  * [Item24908](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24908)
    - Content: 58. Public Works and Transportation Departments briefing regarding the Rainey Street recommendations.

### Executive Session

  * [Item24673](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24673)
    - Content: 59. Discuss legal issues related to Open Government matters (Private consultation with legal counsel - Section 551.071).
  * [Item24674](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24674)
    - Content: 60. Discuss legal issues related to the transition to electing the council from single-member districts (Private consultation with legal counsel - Section 551.071).
  * [Item25064](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25064)
    - Content: 61. Discuss legal issues related to City of Austin 2013 labor negotiations with employees in the Fire, Police, and EMS departments (Private consultation with legal counsel-Section 551.071).

### 2:00 PM - Zoning Ordinances / Restrictive Covenants (HEARINGS CLOSED)

  * [Item25032](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25032)
    - Content: 62. C14-2011-0141 - Peaceful Hill Condominiums - Approve third reading of an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 8107 Peaceful Hill Lane and 501 Hubach Lane (South Boggy Creek Watershed) from development reserve (DR) district zoning to townhouse and condominium residence-conditional overlay (SF-6-CO) combining district zoning, with conditions. First reading approved with conditions on April 26, 2012. Vote: 5-2, Mayor Lee Leffingwell and Council Member Tovo voted nay. Second Reading approved with conditions on June 14, 2012. Vote: 5-2 Council Members Morrison and Tovo voted nay. Owner/Applicant: Kristopher Alsworth, Delton Hubach, Jim Bula and Catherine Christopherson. Agent: The Weichert Law Firm (Glenn K. Weichert). City Staff: Wendy Rhoades, 974-7719.
  * [Item25109](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25109)
    - Content: 63. C14-2012-0083 Cirrus Rezoning Approve third reading of an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 800 West 6th Street and 602-702 West Avenue (Shoal Creek Watershed) from downtown mixed use-conditional overlay-central urban redevelopment (DMU-CO-CURE) combining district zoning for Tract 1 and limited office (LO) district zoning and general office (GO) district zoning for Tract 2 to downtown mixed use-central urban redevelopment (DMU-CURE) combining district zoning for Tract 1 and downtown mixed use-conditional overlay (DMU-CO) combining district zoning for Tract 2. First reading approved on April 25, 2013. Vote 5-1. Council Member Tovo voted nay. Council Member Martinez was absent. Second reading approved on May 23, 2013. Vote 6-0. Mayor Leffingwell was off the dais. Applicant: Cirrus Logic, Inc. (Thurman Case). Agent: Armbrust & Brown, PLLC (Richard Suttle). City Staff: Clark Patterson, 974-7691.

### 2:00 PM - Zoning and Neighborhood Plan Amendments (Public Hearings and Possible Action)

  * [Item24418](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24418)
    - Content: 64. NPA-2012-0023.01 - 1504 East 51st Street (Promiseland) - Conduct a public hearing and approve an ordinance amending Ordinance No. 20070809-55 of the University Hills/Windsor Park Combined Neighborhood Plan, to change the future land use designation on the future land use map (FLUM) on property locally known as 1504 East 51st Street (Tannehill Branch Watershed) from Civic to Mixed Use land use. Staff Recommendation: To grant Mixed Use land use. Planning Commission Recommendation: To grant Mixed Use land use and Open Space land use. Owner: The World of Pentecost Inc./HCM, LLC (Shules Hersh). Applicant and Agent: Hughes Capital Management, Inc. (Trac Bledsoe). City Staff: Maureen Meredith, 974-2695.
  * [Item24648](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24648)
    - Content: 65. NPA-2012-0016.01.SH - 1141 Shady Lane (thinkEAST Austin) - Conduct a public hearing and approve an ordinance amending Ordinance No. 20030327-12 of the Govalle/Johnston Terrace Combined Neighborhood Plan, an element of the Imagine Austin Comprehensive Plan, to change the future land use designation on the future land use map (FLUM) on property locally known as 1411 Shady Lane and 5300 Jain Lane (Boggy Creek Watershed) from Single Family land use and Mixed Use land use to Major Planned Developments land use. Staff Recommendation: To grant Major Planned Developments land use. Planning Commission Recommendation: To grant Major Planned Development land use. Owner: thinkEAST Austin, L.P. Applicant/Agent: thinkEAST Austin Management, L.L.C. (Richard de Varga). City Staff: Maureen Meredith, 974-2695.
  * [Item24649](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24649)
    - Content: 66. C814-2012-0128.SH thinkEAST Austin- Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 1411 Shady Lane and 5300 Jain Lane (Boggy Creek Watershed; Tannehill Branch Watershed) from limited office-mixed use-conditional overlay-neighborhood plan (LO-MU-CO-NP) combining district and family residence-neighborhood plan (SF-3-NP) combining district zoning to planned unit development-neighborhood plan (PUD-NP) combining district zoning. Staff Recommendation: To grant planned unit development-neighborhood plan (PUD-NP) combining district zoning, with conditions. Planning Commission Recommendation: To grant planned unit development-neighborhood plan (PUD-NP) combining district zoning, with conditions.Owner/Agent: thinkEAST Austin, LP (Richard deVarga). City Staff: Heather Chaffin, 974-2122.
  * [Item25056](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25056)
    - Content: 67. NPA-2013-0011.01 4805 Harmon Avenue Conduct a public hearing and approve an ordinance amending Ordinance No. 020523-30, the North Loop Neighborhood Plan, an element of the Imagine Austin Comprehensive Plan, to change the land use designation on the future land use map (FLUM) on property locally known as 4805 Harmon Avenue (Tannehill Branch Watershed) from Commercial land use to Single Family land use. Staff Recommendation: To grant Single Family land use. Planning Commission Recommendation: To grant Single Family land use. Owner/Applicant: Mackey Adams Properties, Inc. (Harry L. Mackey). Agent: Karen Radtke Interior Design (Karen Radtke). City Staff: Maureen Meredith, 974-2695.
  * [Item25051](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25051)
    - Content: 68. C14-2013-0013 4805 Harmon Ave. Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 4805 Harmon Avenue (Boggy Creek Watershed) from limited office-conditional overlay-neighborhood plan (LO-CO-NP) combining district zoning to single family residence-small lot-neighborhood plan (SF-4A-NP) combining district zoning. Staff Recommendation: To grant single family residence-small lot-conditional overlay- neighborhood plan (SF-4A-CO-NP) combining district zoning. Planning Commission Recommendation: To grant single family residence-small lot-conditional overlay-neighborhood plan (SF-4A-CO-NP) combining district zoning. Applicant: Mackey Adams Properties, Inc. (Harry Mackey). Agent: Karen Radtke Interior Design (Karen Radtke). City Staff: Clark Patterson, 974-7691.
  * [Item25057](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25057)
    - Content: 69. NPA-2013-0011.02 4914 Bennett Avenue (Tomlinson s Feed & Pets, Inc.) Conduct a public hearing and approve an ordinance amending Ordinance No. 020523-30, the North Loop Neighborhood Plan, an element of the Imagine Austin Comprehensive Plan, to change the land use designation on the future land use map (FLUM) on property locally known as 4914 Bennett Avenue (Tannehill Branch Watershed) from Single Family land use to Mixed Use land use. Staff Recommendation: To grant Mixed Use/Office land use. Planning Commission Recommendation: To grant indefinite postponement by Applicant. Owner/Applicant: Tomlinson s Feed & Pets, Inc. (Scott Click). Agent: Thrower Design (A. Ron Thrower). City Staff: Maureen Meredith, 974-2695.
  * [Item25050](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25050)
    - Content: 70. C14-2013-0021 Tomlinson s Feed & Pets Rezoning Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 4914 Bennett Avenue (Boggy Creek Watershed) from family residence-neighborhood plan (SF-3-NP) combining district zoning to community commercial-mixed use-neighborhood plan (GR-MU-NP) combining district zoning. Staff Recommendation: To grant community commercial-mixed use-conditional overlay-neighborhood plan (GR-MU-CO-NP) combining district zoning. Planning Commission Recommendation: To grant indefinite postponement request by Applicant. Applicant: Tomlinson s Feed & Pets, Inc. (Scott Click). Agent: Thrower Design (Ron Thrower). City Staff: Clark Patterson, 974-7691.
  * [Item25055](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25055)
    - Content: 71. NPA-2013-0025.04 6110 Hill Forest Drive (Beiter-2) Conduct a public hearing and approve an ordinance amending Ordinance No. 20081211-096, the Oak Hill Combined Neighborhood Plan, an element of the Imagine Austin Comprehensive Plan, to change the land use designation on the future land use map (FLUM) on property locally known as 6110 Hill Forest Drive (Williamson Creek Watershed-Barton Springs Zone) from Single Family land use to Higher Density Single Family land use. Staff Recommendation: To grant Higher Density Single Family land use. Planning Commission Recommendation: To grant Higher Density Single Family land use. Owner: Michael and Paulette Beiter. Applicant/Agent: Jim Bennett Consulting (Jim Bennett). City Staff: Maureen Meredith, 974-2695.
  * [Item25059](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25059)
    - Content: 72. C14-2013-0018 Beiter-2 Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 6110 Hill Forest Drive (Williamson Creek Watershed-Barton Springs Zone) from family residence-neighborhood plan (SF-3-NP) combining district zoning to urban family residence-conditional overlay-neighborhood plan (SF-5-CO-NP) combining district zoning. Staff Recommendation: To grant urban family residence-conditional overlay-neighborhood plan (SF-5-CO-NP) combining district zoning. Planning Commission Recommendation: To grant urban family residence-conditional overlay-neighborhood plan (SF-5-CO-NP) combining district zoning, with conditions. Owner/Applicant: Michael and Paulette Beiter. Agent: Jim Bennett Consulting (Jim Bennett). City Staff: Wendy Rhoades, 974-7719.
  * [Item25033](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25033)
    - Content: 73. C14-2012-0140 - Street and Bridge District Office- Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 3511 Manor Road (Tannehill Branch Watershed) from community commercial-neighborhood plan (GR-NP) combining district zoning and community commercial-vertical mixed use building-neighborhood plan (GR-V-NP) combining district zoning to general commercial services-conditional overlay-neighborhood plan (CS-CO-NP) combining district zoning. Staff Recommendation: To grant general commercial services-conditional overlay-neighborhood plan (CS-CO-NP) combining district zoning. Planning Commission Recommendation: To be reviewed on June 25, 2013. Owner/Agent: City of Austin (Peter Davis). City Staff: Heather Chaffin, 974-2122. A valid petition has been filed in opposition to this rezoning request.
  * [Item25034](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25034)
    - Content: 74. C814-2012-0160 - 211 South Lamar Boulevard Planned Unit Development - Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 211 South Lamar Boulevard (Lady Bird Lake Watershed) from general commercial services (CS) district zoning and general commercial services-vertical mixed use building (CS-V) combining district zoning to planned unit development (PUD) district zoning. Staff Recommendation: To grant planned unit development (PUD) district zoning. Planning Commission Recommendation: To be reviewed June 11, 2013. Owner: Post Paggi, LLC (Jason Post). Applicant: Winstead PC (Stephen O. Drenner). City Staff: Lee Heckman, 974-7604.
  * [Item24650](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24650)
    - Content: 75. C14-2013-0011 - Restaurant - Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by zoning property locally known as 11800 Arabian Trail (Walnut Creek Watershed) from single family residence-standard lot (SF-2) district zoning to community commercial-conditional overlay (GR-CO) combining district zoning. Staff Recommendation: To deny community commercial-conditional overlay (GR-CO) combining district zoning. Zoning and Platting Commission Recommendation: To be reviewed on June 4, 2013. Owner/Applicant: Xue Yun Tang. Agent: Shaw Hamilton Consultants (Shaw Hamilton). City Staff: Sherri Sirwaitis, 974-3057.
  * [Item25035](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25035)
    - Content: 76. C14-2013-0020 - Oak Creek Village - Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 2324 Wilson Street (Bouldin Creek Watershed) from multi-family residence medium density-neighborhood plan (MF-3-NP) combining district zoning to multi-family residence-highest density-conditional overlay-neighborhood plan (MF-6-CO-NP) combining district zoning. Staff Recommendation: To grant multi-family residence highest density-conditional overlay-neighborhood plan (MF-6-CO-NP) combining district zoning with conditions. Planning Commission Recommendation: To grant multi-family residence-highest density-conditional overlay-neighborhood plan (MF-6-CO-NP) combining district zoning with conditions. Owner: 2007 Travis Heights, LP (Rene D. Campos). Applicant/Agent: Winstead PC (John Donisi). City Staff: Lee Heckman, 974-7604.
  * [Item25062](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25062)
    - Content: 77. C14-2013-0031 Clawson Patio Homes Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 3907 Clawson Road (West Bouldin Creek Watershed) from family residence (SF-3) district zoning to urban family residence (SF-5) district zoning. Staff Recommendation: To grant urban family residence-conditional overlay (SF-5-CO) combining district zoning with conditions. Planning Commission Recommendation: To be reviewed on June 25, 2013. Owner: Dean Chen. Applicant: Bleyl Interests, Inc. (Vincent G. Huebinger). City Staff: Lee Heckman, 974-7604.
  * [Item25063](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25063)
    - Content: 78. C14-2013-0032 Clawson Patio Homes II Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 3903 Clawson Road (West Bouldin Creek Watershed) from family residence (SF-3) district zoning to multi-family residence-limited density (MF-1) district zoning. Staff Recommendation: To grant multi-family residence-limited density-conditional overlay (MF-1-CO) combining district zoning with conditions. Planning Commission Recommendation: To be reviewed on June 25, 2013. Owner: Roy G. Crouse. Applicant: Bleyl Interests, Inc. (Vincent G. Huebinger). City Staff: Lee Heckman, 974-7604.
  * [Item25061](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25061)
    - Content: 79. C14-2013-0037 7509 Manchaca Office Park Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 7509 Manchaca Road (Williamson Creek Watershed) from warehouse/limited office-conditional overlay (W/LO-CO) combining district zoning to limited office (LO) district zoning. Staff Recommendation: To grant limited office-conditional overlay (LO-CO) combining district zoning. Zoning and Platting Commission Recommendation: To grant limited office-conditional overlay (LO-CO) combining district zoning. Owner/Applicant: 7509 Manchaca, LLC (Mervin Fatter). Agent: Fatter & Evans, Architect (Mervin Fatter). City Staff: Wendy Rhoades, 974-7719.
  * [Item25054](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25054)
    - Content: 80. C14-2013-0041 Fort Dessau-GR Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 1602 Fish Lane (Harris Branch Watershed) from single family residence-standard lot-conditional overlay (SF-2-CO) combining district zoning to multi-family residence-medium density (MF-3) district zoning. Staff Recommendation: To grant multi-family residence-medium density (MF-3) district zoning. Zoning and Platting Commission Recommendation: To be reviewed on June 18, 2013. Owner/Applicant: John C. & Dana Fish. Agent: Land Strategies, Inc. (Paul W. Linehan). City Staff: Sherri Sirwaitis, 974-3057.
  * [Item25053](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25053)
    - Content: 81. C14-2013-0042 Fort Dessau-P Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 13826 Dessau Road (Harris Branch Watershed) from single family residence-standard lot-conditional overlay (SF-2-CO) combining district zoning to public (P) district zoning. Staff Recommendation: To grant public (P) district zoning. Zoning and Platting Commission Recommendation: To grant public (P) district zoning. Owner/Applicant: John C. & Dana Fish. Agent: Land Strategies, Inc. (Paul W. Linehan). City Staff: Sherri Sirwaitis, 974-3057.
  * [Item25052](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25052)
    - Content: 82. C14-2013-0043 Fort Dessau-SF-6 Conduct a public hearing and approve an ordinance amending Chapter 25-2 of the Austin City Code by rezoning property locally known as 13826 Dessau Road (Harris Branch Watershed) from neighborhood commercial (LR) district zoning to townhouse & condominium residence (SF-6) district zoning. Staff Recommendation: To grant townhouse & condominium residence (SF-6) district zoning. Zoning and Platting Commission Recommendation: To grant townhouse & condominium residence (SF-6) district zoning. Owner/Applicant: John C. & Dana Fish. Agent: Land Strategies, Inc. (Paul W. Linehan). City Staff: Sherri Sirwaitis, 974-3057.

### 3:00 PM - Austin Housing and Finance Corporation Meeting

  * [Item25098](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25098)
    - Content: 83. The Mayor will recess the City Council meeting to conduct a Board of Directors' Meeting of the Austin Housing Finance Corporation. Following adjournment of the AHFC Board meeting the City Council will reconvene. (The AHFC agenda is temporarily located at https://austin.siretechnologies.com/sirepub/mtgviewer.aspx?meetid=420&doctype=AGENDA).

### 4:00 PM - Public Hearings and Possible Actions

  * [Item24407](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24407)
    - Content: 84. Conduct a public hearing and consider an ordinance amending City Code Chapter 25-2 (Zoning) to create the Central Austin - University Area Zoning Overlay District in which a group residential land use is a conditional use in the multi-family residence moderate-high density (MF-4) base zoning district.
  * [Item24424](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24424)
    - Content: 85. Conduct a public hearing and consider an ordinance amending City Code Chapter 25-2, Subchapter C, Article 3, Division 9 (University Neighborhood Overlay District Requirements) relating to affordable housing regulations in the university neighborhood overlay (UNO) district.
  * [Item24458](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24458)
    - Content: 86. Conduct a public hearing and consider an ordinance amending City Code Chapters 8-1 and 25-6 to authorize parking utilization agreements for certain under-used parking lots on city parkland, in exchange for funding to provide significant amenities or improvements to serve the park.
  * [Item24501](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item24501)
    - Content: 87. Conduct a public hearing and consider an ordinance amending City Code Chapter 25-2 relating to the use classifications of electronic prototype assembly and electronic testing in the downtown mixed use and central business district base zoning districts.
  * [Item25039](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25039)
    - Content: 88. Conduct a public hearing and consider an ordinance amending City Code Chapter 25-2, Subchapter E, relating to design standards and mixed use for development projects.
  * [Item25041](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25041)
    - Content: 89. Conduct a public hearing and consider an ordinance repealing and replacing Article 1 of City Code Chapter 25-12 to adopt the 2012 International Building Code and local amendments.
  * [Item25042](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25042)
    - Content: 90. Conduct a public hearing and consider an ordinance repealing and replacing Article 5 of City Code Chapter 25-12 to adopt the 2012 Uniform Mechanical Code and local amendments.
  * [Item25043](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25043)
    - Content: 91. Conduct a public hearing and consider an ordinance repealing and replacing Article 12 of City Code Chapter 25-12 to adopt the 2012 International Energy Conservation Code and local amendments.
  * [Item25044](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25044)
    - Content: 92. Conduct a public hearing and consider an ordinance repealing and replacing City Code Chapter 25-12, Article 7 to adopt the 2012 International Fire Code and local amendments.
  * [Item25049](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=403&agviewdoctype=AGENDA#Item25049)
    - Content: 93. Conduct a public hearing and consider an ordinance repealing and replacing Article 6 of City Code Chapter 25-12 to adopt the 2012 Uniform Plumbing Code and local amendments.


# [Meeting 420](http://austin.siretechnologies.com/sirepub/mtgviewer.aspx?meetid=420&amp;doctype=agenda)

## [Agenda 2013-06-06](http://austin.siretechnologies.com/sirepub/agview.aspx?agviewmeetid=420&agviewdoctype=AGENDA)

  No categorized agenda items found
