#! /usr/bin/env ruby

require 'rubygems'
require 'open-uri'
require 'nokogiri'

module WebHelper
  def html(url)
    Nokogiri::HTML(open(url))
  end

  def webpage(url)
    puts "Fetching url #{url}..."
    webpage = ""
    begin
      webpage = open(URI.escape(url), 'User-Agent' => 'ruby-wget').read
    rescue RuntimeError => e
      puts "The request for a page at #{url} failed because of a problem with the OpenURI gem."
      if (e.message =~ /redirection forbidden/)
        puts "You are trying to access a resource that requires redirection to another site."
        puts e.backtrace
      end
    rescue SocketError => e
      puts "The request for a page at #{url} could not be completed."
    rescue Timeout::Error => e
      puts "The request for a page at #{url} timed out...skipping."
    rescue OpenURI::HTTPError => e
      puts "The request for a page at #{url} returned an error. #{e.message}"
    end
    webpage
  end
end

