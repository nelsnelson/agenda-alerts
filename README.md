# Agenda Alerts

This is a bot and some helpers for scanning the Austin City Council meetings agenda page format.

### Meetings

Run ./meetings_helper.rb to acquire link to current and upcoming meetings.

### Data

Run ./data_helper.rb to spider over an upcoming meeting index for new agenda items.

### Bots

Run ./bot-redis.rb to load upcoming meeting agenda items into a running redis server.

Run ./bot-mongo.rb to load upcoming meeting agenda items into a running mongo server.


