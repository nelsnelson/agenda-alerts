#! /usr/bin/env ruby

require 'chronic'

DATE = '((Mon|Tues|Wednes|Thurs|Fri|Satur|Sun)day, ' + 
       '(January|February|March|April|May|June|July|August|September|October|November|December) ' +
       '\d\d, \d\d\d\d)'

CONVENE = 'will convene at .* on (' + DATE + ')'

module AgendaTimeHelper
  def agenda_time(agenda_url)
    time = "January 1, 1970 00:00 UTC"
    page = html agenda_url
    spans = page.css('p').css('span')

    spans.each { |span|
      span_text = span.text.strip.gsub(/\r?\n/, ' ').gsub(/\s+/, ' ').strip

      if span_text.bytes.entries != [ 194, 160 ]
        if span_text.match(/#{CONVENE}/)
          time = Chronic.parse("#{$1}")
          break
        elsif span_text.match(/#{DATE}/)
          time = Chronic.parse("#{$1}")
          break
        end
      end
    }
    time
  end
end
