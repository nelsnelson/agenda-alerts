#! /usr/bin/env ruby

require './meetings_helper.rb'
require './time_helper.rb'
require './zipcode_helper.rb'
require './docs_helper.rb'

# <p class=MsoNormal><a name="Item24882"><span style=\'font-size:10.0pt;font-family:"Garamond","serif"\'>(.*)</span></a></p>
# PARAGRAPH = '<p class=MsoNormal><a name="(Item\d+)"><span style=.*>(.*)</span></a></p>'
TABLE = '<table.*>(.*)<\/table>'
TYPE = '<p class="?MsoNormal"?><a name="?(Item\d+)"?><b><span style=\'.*\'>(.*)<\/span><\/b><\/a><\/p>'
ITEM = '<a name="?(Item\d+)"?><span style=\'.*\'>(.*)<\/span><\/a>'

module ItemsHelper
  include WebHelper
  include AgendaTimeHelper
  include ZipcodeHelper
  include DocsHelper

  HEADERS_TO_IGNORE = [
    'The City of Austin is committed to compliance with the Americans with Disabilities Act',
    'City Countil Convenes',
    'Invocation', 
    'Consent',
    'Approval of Minutes',
    'Non-Consent',
    'Adjourn',
    'Executive Session'
  ]
  HEADERS_TO_IGNORE_REGEXP = Regexp.new("(#{HEADERS_TO_IGNORE.join('|')})")

  def agenda_items(meeting)
    results = []
    agenda_url = meeting[:agenda_url]
    page = html agenda_url
    time = agenda_time agenda_url
    spans = page.css('p').css('span')
    tables = page.css('table')
    rows = tables.css('tr')
    type_found = false
    type = nil
    rows.each { |row|
      columns = row.css('td').entries
      type_found = columns.any? { |column| column.text.strip.bytes.entries == [ 194, 160 ] }
      if type_found
        entry = Hash.new
        type = columns.map(&:text).map(&:strip).join(' ').gsub(/\r?\n/, ' ').gsub(/\r?\n  /, ' ')
        type.gsub!(/\s+/, ' ')
        type = type.scan(/[[:print:]]/).join('').gsub(/\s+/, ' ').strip
      else
        next if type =~ /Approval of Minutes/
        raw_html_item_content = columns.map(&:to_s).join("\n")
        content = columns.map(&:text).map(&:strip).join(' ')
        content = content.gsub(/\r?\n  /, ' ').gsub(32.chr, ' ').scan(/[[:print:]]/).join('').gsub(/\s+/, ' ').strip
        next if HEADERS_TO_IGNORE_REGEXP.match content
        anchor = Nokogiri::HTML::DocumentFragment.parse(raw_html_item_content).css('p.MsoNormal').at('a')
        zipcode = zipcode(raw_html_item_content)
        item_id = anchor ? anchor['name'] : ''
        next if item_id.empty?
        doc_urls = docs item_id.gsub(/[^\d]/, '')
        next if type.empty?
        next unless type
        next unless doc_urls
        puts "Item #{item_id} doc urls: #{doc_urls}"
        item = {
          :agenda_id => meeting[:agenda_id],
          :id => item_id,
          :content => content,
          :category => type,
          :zipcode => zipcode,
          :doc_urls => doc_urls
        }
        item[:url] = "#{agenda_url}##{item_id}" if anchor
        results << item
        type_found = columns.any? { |column| column.text.strip.bytes.entries == [ 194, 160 ] }
      end
    }
    results
  end
end

class App
  include MeetingsHelper
  include ItemsHelper

  def main
    meetings.each { |meeting|
      puts
      puts
      puts "# [Meeting #{meeting[:id]}](#{meeting[:url]})"
      puts
      puts "## [Agenda #{meeting[:agenda_id]}](#{meeting[:agenda_url]})"
      puts
      items = agenda_items meeting
      puts "  No categorized agenda items found" if items.empty? or items.all? { |item| item[:category].empty? }
      category = nil
      items.each { |item|
        if category != item[:category]
          category = item[:category]
          unless category.empty?
            puts
            puts "### #{category}"
            puts
          end
        end
        puts "  * " + (item[:url] ? "[#{item[:id]}](#{item[:url]})" : item[:id])
        puts "    - Content: #{item[:content]}"
        puts "    - Zip code: [#{item[:zipcode]}](https://maps.google.com/maps?q=#{item[:zipcode]})" if item[:zipcode]
      }
    }
  rescue Interrupt => ex
    puts "Done"
  end
end

App.new.main if __FILE__ == $0

