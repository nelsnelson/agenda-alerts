#! /usr/bin/env ruby

ZIPCODE = '(Austin, Texas (\d{5}))'

module ZipcodeHelper
  def zipcode(raw_html_agenda_item_content)
    zipcode = nil
    fragment = Nokogiri::HTML::DocumentFragment.parse(raw_html_agenda_item_content)#.css('p.MsoNormal').at('a')
    spans = fragment.css('p').css('span')

    spans.each { |span|
      span_text = span.text.strip.gsub(/\r?\n/, ' ').gsub(/\s+/, ' ').strip
      if span_text.bytes.entries != [ 194, 160 ]
        if span_text.match(/#{ZIPCODE}/)
          zipcode_match = "#{$2}"
          zipcode = zipcode_match unless zipcode_match.empty?
          break
        end
      end
    }
    zipcode
  end
end
